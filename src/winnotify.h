#ifndef __WINNOTIFY_H__
#define __WINNOTIFY_H__

#include <windows.h>
#include <shellapi.h>

class WinNotifyEvent
{
    public:
        virtual \
            void OnNotiEvent( unsigned tp, unsigned wp, unsigned lp ) = 0;
};

class WinNotify
{
    public:
        WinNotify( HINSTANCE hIparent, HWND hWparent, HICON hIcon,
                   WinNotifyEvent* e = NULL );
        ~WinNotify();

    public:
        void tooltip( const wchar_t* msg );
        void pushMsg( const wchar_t* title, const wchar_t* msg, unsigned ms  = 3000 );

    public: /// do not call directly.
        void MsgEvt( unsigned* p );

    public:
        WinNotifyEvent* event() { return evt; }
        void event( WinNotifyEvent* e ) { evt = e; }
        HWND handle() { return hHiddenWin; }

    protected:
        bool createHiddenChildWindow();
        bool destroyHiddenChildWindow();
        
    protected:
        WinNotifyEvent*     evt;
        HINSTANCE           hInstParent;
        HWND                hWndParent;
        HWND                hHiddenWin;
        HWND                hToolinfo;
        HICON               hNotiIcon;
        NOTIFYICONDATA      notidata;
        wchar_t             tipdata[128];
};
#endif /// of __WINNOTIFY_H__
