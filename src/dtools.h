#ifndef __DTOOLS_H__
#define __DTOOLS_H__

/*
* Directory tools for Windows with wide characters.
*/

#include <string>
#include <vector>

const \
  wchar_t*  platformdirseperator();
bool        testDirectory( const wchar_t* path );
bool        createDirectory( const wchar_t* path );
unsigned    searchFiles( const wchar_t* fpath, std::vector<std::wstring> &files, const wchar_t* ext = NULL, bool recursive = false );
bool        removeFilesInDir( const wchar_t* fpath, const wchar_t* ext = NULL );
void        makeDirPlatformize( std::wstring &path );
void        removeLastDirSep( std::wstring &path );
const \
  wchar_t*  stripFilePath( const wchar_t* refpath );
const \
  wchar_t*  stripFileName( const wchar_t* refpath );
const \
  wchar_t*  removeFilePath( const wchar_t* file_path );

#endif // __DTOOLS_H__
