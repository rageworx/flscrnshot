/*
* This project designed for Windows.
**/
#include <windows.h>
#include <unistd.h>

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <string>
#include <cstdint>
#include "winMain.h"

#include "fl_imgtk.h"
#include <FL/platform.H>
#include <FL/Fl_Copy_Surface.H>
#include <FL/fl_draw.H>
#include <FL/fl_ask.H>
#if defined(USE_SYSPNG)
    #include <png.h>
#else
    #include <FL/images/png.h>
#endif /// of defined(USE_SYSPNG)
#include "turbojpeg.h"
#include <omp.h>

#include "resource.h"
#include "dtools.h"
#include "ltools.h"

#include <FL/Fl_BMP_Image.H>
#include <FL/Fl_PNG_Image.H>
#include <FL/Fl_JPEG_Image.H>
#include <FL/Fl_Native_File_Chooser.H>

#include "wintk.h"

////////////////////////////////////////////////////////////////////////////////

using namespace std;

////////////////////////////////////////////////////////////////////////////////

#define DEF_APP_NAME            "FLTK Screenshot for Win64"
#define DEF_WIDGET_FSZ          12

#ifdef __linux__
#define DEF_WIDGET_FNT          FL_HELVETICA
#else
#define DEF_WIDGET_FNT          FL_FREE_FONT
#endif // __linux__

#define DEF_WIN_COLOR_BG        0x33333300
#define DEF_WIN_COLOR_FG        0xFFFFFF00

#define APPLY_THEME_OUT( _x_ )  _x_->box( FL_THIN_UP_BOX );\
                                _x_->color( fl_darker( window->color() ) );\
                                _x_->labelcolor( window->labelcolor() );\
                                _x_->textcolor( 0xFF663300 );\
                                _x_->textsize( window->labelsize() );\
                                _x_->labelfont( window->labelfont() );\
                                _x_->labelsize( window->labelsize() );\
                                _x_->callback( WMain::WidgetCB, this );\
                                _x_->clear_visible_focus()
#define APPLY_THEME_BTN( _x_ )  _x_->color( window->color(), \
                                       fl_darker( window->color() ) );\
                                _x_->labelcolor( window->labelcolor() );\
                                _x_->labelfont( window->labelfont() );\
                                _x_->labelsize( window->labelsize() );\
                                _x_->callback( WMain::WidgetCB, this );\
                                _x_->clear_visible_focus()
#define APPLY_THEME_BOX( _x_ )  _x_->color( window->color() );\
                                _x_->labelcolor( window->labelcolor() );\
                                _x_->labelfont( window->labelfont() );\
                                _x_->labelsize( window->labelsize() )
#define WRITESTATUS( _x_ )      laststatus = _x_;\
                                boxLastStatus->label( laststatus.c_str() );\
                                if ( grpMain->active_r() > 0 ) \
                                    boxLastStatus->redraw()

#define APPLY_THEME_BTN2( _x_ )  _x_->color( winUtil->color(), \
                                        fl_darker( winUtil->color() ) );\
                                 _x_->labelcolor( winUtil->labelcolor() );\
                                 _x_->labelfont( winUtil->labelfont() );\
                                 _x_->labelsize( winUtil->labelsize() );\
                                 _x_->callback( WMain::WidgetCB, this );\
                                 _x_->clear_visible_focus()
#define APPLY_THEME_BOX2( _x_ )  _x_->color( winUtil->color() );\
                                 _x_->labelcolor( winUtil->labelcolor() );\
                                 _x_->labelfont( winUtil->labelfont() );\
                                 _x_->labelsize( winUtil->labelsize() )
#define APPLY_THEME_CHOICE( _x_ )   _x_->box( FL_NO_BOX );\
                                    _x_->color( window->color() );\
                                    _x_->labelcolor( window->labelcolor() );\
                                    _x_->labelsize( window->labelsize() );\
                                    _x_->textcolor( 0xFF663300 );\
                                    _x_->textsize( window->labelsize() );\
                                    _x_->textfont( window->labelfont() );\
                                    _x_->when( FL_WHEN_CHANGED );\
                                    _x_->callback( WMain::WidgetCB, this );\
                                    _x_->clear_visible_focus()
#define APPLY_THEME_SLIDER( _x_ ) _x_->align( FL_ALIGN_LEFT );\
                                  _x_->labelfont( window->labelfont() );\
                                  _x_->labelsize( window->labelsize() );\
                                  _x_->labelcolor( window->labelcolor() );\
                                  _x_->color( 0x554433FF, 0x887766FF );\
                                  _x_->when( FL_WHEN_CHANGED );\
                                  _x_->callback( WMain::WidgetCB, this );\
                                  _x_->clear_visible_focus()

////////////////////////////////////////////////////////////////////////////////

typedef int(__stdcall *MSGBOXWAPI)(IN HWND, IN LPCWSTR, IN LPCWSTR, IN UINT, IN WORD, IN DWORD);

////////////////////////////////////////////////////////////////////////////////

static string resourcebase;
extern HINSTANCE fl_display;

////////////////////////////////////////////////////////////////////////////////

bool getResource( const char* scheme, uchar** buff, uint32_t* buffsz )
{
    wchar_t convscheme[80] = {0,};

    fl_utf8towc( scheme, strlen(scheme), convscheme, 80 );

    HRSRC rsrc = FindResource( NULL, convscheme, RT_RCDATA );
    if ( rsrc != NULL )
    {
        *buffsz = SizeofResource( NULL, rsrc );
        if ( *buffsz > 0 )
        {
            HGLOBAL glb = LoadResource( NULL, rsrc );
            if ( glb != NULL )
            {
                void* fb = LockResource( glb );
                if ( fb != NULL )
                {
                    uchar* cpbuff = new uchar[ *buffsz ];
                    if ( cpbuff != NULL )
                    {
                        memcpy( cpbuff, fb, *buffsz );

                        *buff = cpbuff;

                        UnlockResource( glb );
                        return true;
                    }

                    UnlockResource( glb );
                }
            }
        }
    }

    return false;
}

Fl_RGB_Image* createResImage( const char* scheme )
{
    if ( scheme != NULL )
    {
        uchar* buff = NULL;
        uint32_t buffsz = 0;

        if ( getResource( scheme, &buff, &buffsz ) == true )
        {
            Fl_RGB_Image* retimg = new Fl_PNG_Image( scheme, buff, buffsz );
            delete[] buff;

            return retimg;
        }
    }

    return NULL;
}

int X_MessageBoxTimeout(HWND hWnd, const TCHAR* sText, const TCHAR* sCaption, UINT uType, DWORD dwMilliseconds)
{
    int retI = 0;

    static MSGBOXWAPI MessageBoxTimeoutW = NULL;

    HMODULE hUser32 = NULL;
    bool    loaded = false;

    if ( MessageBoxTimeoutW == NULL )
    {
        hUser32 = LoadLibraryA( "user32.dll" );
    }

    if ( MessageBoxTimeoutW == NULL )
    {
        if ( hUser32 != NULL )
        {
            MessageBoxTimeoutW = \
                (MSGBOXWAPI)GetProcAddress(hUser32, "MessageBoxTimeoutW");
        }
    }

    if ( MessageBoxTimeoutW != NULL )
    {
        retI = MessageBoxTimeoutW( hWnd, 
                                   sText, 
                                   sCaption, 
                                   uType, 
                                   0, 
                                   dwMilliseconds);
        if( hUser32 != NULL )
            FreeLibrary( hUser32 );
    }
    else
    {
        retI = MessageBox(hWnd, sText, sCaption, uType);
    }

    return retI;
}

const char* _ftoa( float f )
{
    static char fstr[80] = {0};
    sprintf( fstr, "%.2f", f );
    return fstr;
}

const char* _wc2utf8( const wchar_t* src )
{
    static char retstr[512] = {0};

    memset( retstr, 0, 512 );

    fl_utf8fromwc( retstr, 512, src, wcslen( src ) );

    return retstr;
}

const wchar_t* _utf8_2wc( const char* src )
{
    static wchar_t retstr[512] = {0};

    memset( retstr, 0, 512 * sizeof(wchar_t) );

    fl_utf8towc( src, strlen( src ), retstr, 512 );

    return retstr;
}

static Fl_RGB_Image* to_flrgb( HDC dc, HBITMAP hBmp )
{
    BITMAPINFO bmi = {0};
    bmi.bmiHeader.biSize = sizeof( bmi.bmiHeader );

    //Get info.
    int ret = \
    GetDIBits( dc,
               hBmp, 
               0, 0, NULL, &bmi, 
               DIB_RGB_COLORS );

    if ( ret == 0 )
        return NULL;

    int w = bmi.bmiHeader.biWidth;
    int h = bmi.bmiHeader.biHeight;

    uint8_t* buf = new uint8_t[ w * h * 4 ];

    if ( buf == NULL )
        return NULL;

    bmi.bmiHeader.biBitCount = 32;
    bmi.bmiHeader.biCompression = BI_RGB;

    ret = \
    GetDIBits( dc, 
               hBmp, 
               0, h, buf, &bmi, 
               DIB_RGB_COLORS );

    if ( ret == 0 )
        return NULL;

    uint32_t bufsz = w * h;
    uint8_t* convbuf = new uint8_t[bufsz*3];
    
    if ( convbuf == NULL )
    {
        delete[] buf;
        return NULL;
    }

    memset( convbuf, 0, bufsz );

    #pragma omp parallel for shared( convbuf, buf )
    for( uint32_t cnty=0; cnty<h; cnty++ )
    {
        for( uint32_t cntx=0; cntx<w; cntx++ )
        {
            uint32_t bmpy = h - cnty -1;
            uint32_t rgbq = ( ( cnty * w ) + cntx ) * 3;
            uint32_t bmpq = ( ( bmpy * w ) + cntx ) * 4;

            convbuf[ rgbq + 0 ] = buf[ bmpq + 2 ];
            convbuf[ rgbq + 1 ] = buf[ bmpq + 1 ];
            convbuf[ rgbq + 2 ] = buf[ bmpq + 0 ];
        }
    }

    Fl_RGB_Image* pImage = new Fl_RGB_Image( convbuf, w, h, 3 );
    if ( pImage != NULL )
    {
        pImage->alloc_array = 1;
    }

    delete[] buf;
    return pImage;
}

static bool BitmapToClipboard( HBITMAP hBMP, HWND hWnd = NULL )
{
    if ( OpenClipboard(hWnd) == FALSE )
        return false;

    EmptyClipboard();

    BITMAP bm;
    GetObject( hBMP, sizeof(bm), &bm );

    BITMAPINFOHEADER bi;
    ZeroMemory(&bi, sizeof(BITMAPINFOHEADER));
    bi.biSize = sizeof(BITMAPINFOHEADER);
    bi.biWidth = bm.bmWidth;
    bi.biHeight = bm.bmHeight;
    bi.biPlanes = 1;
    bi.biBitCount = bm.bmBitsPixel;
    bi.biCompression = BI_RGB;

    if (bi.biBitCount <= 1) // make sure bits per pixel is valid
        bi.biBitCount = 1;
    else if (bi.biBitCount <= 4)
        bi.biBitCount = 4;
    else if (bi.biBitCount <= 8)
        bi.biBitCount = 8;
    else // if greater than 8-bit, force to 24-bit
        bi.biBitCount = 24;

    // Get size of color table.
    SIZE_T dwColTableLen = (bi.biBitCount <= 8) ? (1 << bi.biBitCount) * sizeof(RGBQUAD) : 0;

    // Create a device context with palette
    HDC hDC = GetDC(NULL);
    HPALETTE hPal = static_cast<HPALETTE>(::GetStockObject(DEFAULT_PALETTE));
    HPALETTE hOldPal = ::SelectPalette(hDC, hPal, FALSE);
    RealizePalette(hDC);

    GetDIBits( hDC, hBMP, 0, bi.biHeight, NULL,
               (LPBITMAPINFO)&bi, DIB_RGB_COLORS);

    if ( bi.biSizeImage == 0 )
    {
        bi.biSizeImage = ((((bi.biWidth * bi.biBitCount) + 31) & ~31) / 8) * bi.biHeight;
    }

    HGLOBAL hDIB = GlobalAlloc( GMEM_MOVEABLE,
                                sizeof(BITMAPINFOHEADER) + dwColTableLen + bi.biSizeImage );
    if ( hDIB != NULL )
    {
        union tagHdr_u
        {
            LPVOID             p;
            LPBYTE             pByte;
            LPBITMAPINFOHEADER pHdr;
            LPBITMAPINFO       pInfo;
        } Hdr;

        Hdr.p = ::GlobalLock(hDIB);

        CopyMemory(Hdr.p, &bi, sizeof(BITMAPINFOHEADER));

        int nConv = GetDIBits( hDC, hBMP, 0, bi.biHeight,
                               Hdr.pByte + sizeof(BITMAPINFOHEADER) + dwColTableLen,
                               Hdr.pInfo, DIB_RGB_COLORS );
        GlobalUnlock(hDIB);
        if (!nConv)
        {
            GlobalFree(hDIB);
            hDIB = NULL;
        }
    }

    if (hDIB)
    {
        SetClipboardData(CF_DIB, hDIB);
    }

    CloseClipboard();
    SelectPalette(hDC, hOldPal, FALSE);
    ReleaseDC(NULL, hDC);

    return true;
}

////////////////////////////////////////////////////////////////////////////////

WMain::WMain( int argc, char** argv, const char* loc )
 :  _argc( argc ),
    _argv( argv ),
    crossfade_idx( 0 ),
    crossfade_ratio( 0.0f ),
    window( NULL ),
    busyflag( false ),
    keycapflag( false ),
    poputilflag( false ),
    goingquit( false ),
    poputilret( -1 ),
    lastkey( 0 ),
    imgOptionBg( NULL ),
    winvisstate( 1 ),
    sndBuffer( NULL ),
    sndBuffersz( 0 ),
    cfgLdr( NULL ),
    keyhooker( NULL ),
    langset( NULL ),
    dpiR( 1.0f )
{
    getEnvironments();

    cfgLdr = new ConfigLoader();
    if ( cfgLdr != NULL )
    {
        cfgLdr->Load();

        if ( cfgLdr->GetLastPath() != NULL )
        {
            pathSave = cfgLdr->GetLastPath();
        }
    }
    else
    {
        // It is a critical error.
        raise( SIGABRT );
    }

    if ( pathSave.size() == 0 )
    {
        // Make default path ...
        pathSave  = pathHome;
        pathSave += L"\\Pictures\\FLSCRNSHOT";
    }

    // Check path 
    checkWritePath();

    memset( threads, 0, sizeof( pthread_t ) * MAX_THREADS_CNT );

    // Load langset.
    wchar_t pwd[512] = {0};
    GetModuleFileName( fl_display, pwd, 512 );
    langset = new LangSet( stripFilePath( pwd ), 
                           _utf8_2wc( loc ) );

    // Create widgets...
    createComponents();
    registerPopMenu();

    // load Windows Title Icons --
    hIconWindowLarge = (HICON)LoadImage( fl_display,
                                         MAKEINTRESOURCE( IDC_ICON_A ),
                                         IMAGE_ICON,
                                         64,
                                         64,
                                         LR_SHARED );

    hIconWindowSmall = (HICON)LoadImage( fl_display,
                                         MAKEINTRESOURCE( IDC_ICON_A ),
                                         IMAGE_ICON,
                                         16,
                                         16,
                                         LR_SHARED );
                                         
    hIconNotifier = (HICON)LoadImage( fl_display,
                                      MAKEINTRESOURCE( IDC_ICON_T ),
                                      IMAGE_ICON,
                                      16,
                                      16,
                                      LR_SHARED );

    if ( window != NULL )
    {
       window->icons( hIconWindowLarge,
                      hIconWindowSmall );
    }
    
    // key hooking --
    keyhooker = new WinKeyHook( fl_display, fl_xid( window ), this );
    
    // notifier ---
    notifier = new WinNotify( fl_display, fl_xid( window ), 
                              hIconNotifier, this );
    if ( notifier != NULL )
    {
        // put name ...
    }

    // Load sound buffer
    getResource( "wav_shutter", &sndBuffer, &sndBuffersz );
    
    // Calc DPI
    HDC hdc = GetDC( NULL );
    float logicalY = GetDeviceCaps( hdc, VERTRES );
    float physicalY = GetDeviceCaps( hdc, DESKTOPVERTRES );
    ReleaseDC( NULL, hdc );

    // Let get DPI ratio for scaled --
    // 100% = 1.0f
    // 125% = 1.25f ...    
    dpiR = physicalY / logicalY;
}

WMain::~WMain()
{
    killThread( THREAD_KILL_ALL );

    // remove noti.
    if ( notifier != NULL )
    {
        delete notifier;
    }

    // stop sound , if it was played.
    sndPlaySound( NULL, SND_ASYNC );
    
    if ( sndBuffer != NULL )
    {
        delete[] sndBuffer;
        sndBuffersz = 0;
    }

    // write last parameters.
    updateconfigs();

    int dt = Fl::screen_num( window->x(), window->y() );
    cfgLdr->SetWindowPos( window->x(),
                          window->y(),
                          dt );

    // Get Window visible state ...
    cfgLdr->SetWindowState( winvisstate );

    cfgLdr->Save();
    delete cfgLdr;

    if ( keyhooker != NULL )
    {
        delete keyhooker;
    }

    if ( imgLogo != NULL )
    {
        boxLogo->deimage();
        fl_imgtk::discard_user_rgb_image( imgLogo );
    }

    removeOptionBg();

    delete langset;
}

int WMain::Run()
{
    if ( window != NULL )
    {
        return Fl::run();
    }

    return 0;
}

void* WMain::PThreadCall( ThreadParam* tparam )
{
    if ( tparam == NULL )
        return NULL;

    long pLL = tparam->paramL;
    bool bnflush = false;

    switch( pLL )
    {
        case 0: /// == capture window.
            if ( captureWindow() == true )
            {
                if ( cfgLdr->GetOptCaptureType() == 0 )
                {
                    playCaptureSound();
                }
                else
                if ( cfgLdr->GetOptNotify() > 0 )
                {
                    notifying();
                }
                else
                {
                    playCaptureSound();
                }
            }
            break;
    }

    window->redraw();
    Fl::awake();

    delete threads[ pLL ];
    threads[ pLL ] = NULL;

    pthread_exit( NULL );

    return NULL;
}

void WMain::WidgetCall( Fl_Widget* w )
{
    if ( w == window )
    {
        if ( keycapflag == true )
            return;

        if ( grpOption->visible_r() > 0 )
        {
            grpOption->hide();
            grpMain->activate();
            updateconfigs();
            cfgLdr->Save();
            // let save memory.
            removeOptionBg();
            return;
        }

        if ( goingquit == false )
            showhideWindow();

        return;
    }

    if ( w == winUtil )
    {
        if ( poputilflag == true )
        {
            poputilflag = false;           
            poputilret = -1;
        }
            
        winUtil->hide();

        return;
    }

    if ( w == btnCapKey )
    {
        btnCapKey->deactivate();
        keycapflag = true;

        return;
    }

    if ( w == btnBrowsePath )
    {
        selectNewPath();
        
        return;
    }

    if ( w == btnOption )
    {
        if ( imgOptionBg != NULL )
        {
            boxOptionBg->image( NULL );
            fl_imgtk::discard_user_rgb_image( imgOptionBg );
        }

        size_t wdg_w = window->w();
        size_t wdg_h = window->h();
        bool   dblsz = false;

        if ( dpiR > 1.0f )
        {
            // High DPI with asmv3-GDIscaling element in 
            // manifest XML makes actual drawing DC to
            // double size, not actual DPI ratio when over 100%.
            // It is a reason for looks clear-graphic scaling by Microsoft.
            wdg_w *= 2;
            wdg_h *= 2;
            dblsz = true;
        }

        Fl::lock();
        window->redraw();
        uchar* widgetbuff = new uchar[ wdg_w * wdg_h * 3 + 1 ];
        if ( widgetbuff != NULL )
        {
            Fl::awake(); // flush internal --

            fl_read_image( widgetbuff, 0, 0, wdg_w, wdg_h );
            imgOptionBg = new Fl_RGB_Image( widgetbuff, wdg_w, wdg_h, 3 );

            if ( imgOptionBg != NULL )
            {
                imgOptionBg->alloc_array = 1;

                if ( dblsz == true )
                {
                    Fl_RGB_Image* tmpSwp = \
                        fl_imgtk::rescale( imgOptionBg,
                                           window->w(),
                                           window->h() );
                    if ( tmpSwp != NULL )
                    {
                        fl_imgtk::discard_user_rgb_image( imgOptionBg );
                        imgOptionBg = tmpSwp;
                    }
                }

                fl_imgtk::brightness_ex( imgOptionBg, -35.0 );
                fl_imgtk::blurredimage_ex( imgOptionBg, 8 );
                boxOptionBg->image( imgOptionBg );
            }
            else
            {
                printf( "fl_read_image failure ?\n" );
                fflush( stdout );
            }
        }

        grpMain->deactivate();
        grpOption->show();
        window->redraw();
        Fl::unlock();

        return;
    }

    if ( w == outCapPath )
    {
        const char* data = outCapPath->value();
        if ( data != NULL )
        {
            int datalen = strlen( data );
            if ( datalen > 0 )
            {
                outCapPath->mark( 0 );
                outCapPath->insert_position( datalen );
                // copy text to clipboard.
                Fl::copy( data, datalen, 1 );
            }
        }
        
        return;
    }

    if ( w == btnUtilYes )
    {
        if ( poputilflag == true )
        {
            poputilflag = false;
            poputilret = 0;

            winUtil->hide();
            window->hide();
        }
        else
        {
            winUtil->hide();
        }

        return;
    }

    if ( w == btnUtilNo )
    {
        if ( poputilflag == true )
        {
            poputilflag = false;
            poputilret = 1;
        }

        winUtil->hide();

        return;
    }

    if ( w == chcCaptureType )
    {
        return;
    }

    if ( w == chcImageType )
    {
        int prevq = sldImageQuality->value();

        switch( chcImageType->value() )
        {
            case 0: /// PNG
                {
                    int newq = 0;

                    if ( prevq > 10 )
                        newq = 7; /// default is level 7.

                    sldImageQuality->range( 1, 8 );
                    sldImageQuality->step( 1.0 );
                    sldImageQuality->value( newq );
                }
                break;

            case 1: /// JPEG
                {
                    int newq = 0;

                    if ( prevq < 10 )
                        newq = 91; /// default is 91%.

                    sldImageQuality->range( 10, 100 );
                    sldImageQuality->step( 1.0 );
                    sldImageQuality->value( newq );
                }
                break;
        }

        updateimagequality();
        return;
    }

    if ( w == sldImageQuality )
    {
        updateimagequality();
        return;
    }

    if ( w == popMenu )
    {
        int pmv = popMenu->value();

        switch( pmv )
        {
            case 0: // Hide / show window
                {
                    showhideWindow();
                }
                break;

            case 1: // Reload config
                reloadConfigs();
                break;

            case 2: // Quit.
                goingquit = true;
                window->hide();
                break;
        }

        return;
    }
}

void WMain::OnKeyEvent( uint32_t tp, uint32_t wp, uint32_t lp )
{
    if ( wp == WM_KEYUP )
    {
        // it should be handle extended key in the future.
        uint32_t masklk = (lp & 0x0000FFFF);
        uint32_t masksc = (lp & 0x00FF0000) >> 16;
        uint32_t maskhk = (lp & 0xFF000000) >> 24;

#ifdef KEY_DEBUG
        printf( "//// OnKeyDebug///\n" );
        printf( "[%08X] :: mask keys, h=%02X, sc=%02X, lk=%02X\n", 
                lp,
                maskhk, masksc, masklk );
        fflush( stdout );
#endif
        lastkey = lp;

        // process lp to WM_VK ...
        if ( keycapflag == true )
        {
            if ( lp != VK_ESCAPE )
            {
                // will sens special keys for the future.
                keycapflag = false;

                // store key.
                cfgLdr->SetCapKey( lastkey );
                cfgLdr->Save();
                updateCapKey();
            }
            else /// == ESCAPE key.
            {
                keycapflag = false;
            }

            btnCapKey->activate();
        }
        else
        if ( cfgLdr->GetCapKey() == lp )
        {
            // Make screen shot in pthread.
            createThread( 0 );
        }
    }
}

void WMain::OnNotiEvent( uint32_t tp, uint32_t wp, uint32_t lp )
{
    if ( tp > 0 )
    {
        switch( wp )
        {
            case WM_LBUTTONUP:
                {
                    showhideWindow();
                }
                break;

            case WM_RBUTTONUP:
                if ( poputilflag == false )
                {
                    int mx = 0;
                    int my = 0;
                    Fl::get_mouse( mx, my );

                    int sx = 0;
                    int sy = 0;
                    int sw = 0;
                    int sh = 0;
                    Fl::screen_work_area( sx, sy, sw, sh, mx, my);

                    int putx = mx;
                    int puty = my;

                    if ( putx + winUtil->w() > sw )
                        putx = sw - winUtil->w();

                    if ( puty + winUtil->h() > sh )
                        puty = sh - winUtil->h();

                    winUtil->resize( putx, puty,
                                     winUtil->w(),
                                     winUtil->h() );
                    poputilret = -1;
                    poputilflag = true;
                    winUtil->show();
                    ShowWindow( fl_xid( winUtil ), SW_SHOW );
                    SetWindowPos( fl_xid( winUtil ),
                                  HWND_TOPMOST,
                                  0,0,0,0,
                                  SWP_NOMOVE | SWP_NOSIZE );
                }
                break;
        }
    }
}

void WMain::getEnvironments()
{
    pathHome        = _wgetenv( L"USERPROFILE" );
    pathSystemBase  = _wgetenv( L"SYSTEMROOT" );
    pathUserData    = _wgetenv( L"LOCALAPPDATA" );
    pathUserRoaming = _wgetenv( L"APPDATA" );
}
        
void WMain::updateCapKey()
{
    static char testkeyl[32] = {0};
    static char printkey[64] = {0};

    uint32_t ck = cfgLdr->GetCapKey();
    uint32_t ckl = (ck & 0x0000FFFF);
    uint32_t cks = (ck & 0x00FF0000) >> 16;
    uint32_t ckh = (ck & 0xFF000000) >> 24;

#ifdef KEY_DEBUG
    printf( "////updateCapKey()/////\n" );
    printf( "ck = %08X\n", ck );
    printf( "ckh = %X, ckl = %X\n", ckh, ckl ); fflush( stdout );
#endif

    UINT vkxl = MapVirtualKey( ckl, MAPVK_VK_TO_VSC );

    GetKeyNameTextA( vkxl<<16, testkeyl, 32 );

    if ( strlen( testkeyl ) == 0 )
    {
        sprintf( testkeyl, "VKEY(%u)", ckl );
    }

    memset( printkey, 0, 64 );
    sprintf( printkey, "%s", testkeyl );

    outCapKey->value( printkey );
    outCapKey->redraw();
}

void WMain::setdefaultwintitle()
{
    memset( wintitlestr, 0, MAX_WINTITLE_LEN );
    sprintf( wintitlestr, "%s v.%s", DEF_APP_NAME, APP_VERSION_STR );
}

void WMain::resetParameters()
{
}

void WMain::createComponents()
{
    setdefaultwintitle();
   
    uint32_t ww = 600;
    uint32_t wh = 150;

    window = new Fl_Double_Window( ww, wh, wintitlestr );
    if ( window != NULL )
    {
        window->color( DEF_WIN_COLOR_BG );
        window->labelfont( DEF_WIDGET_FNT );
        window->labelsize( DEF_WIDGET_FSZ );
        window->labelcolor( DEF_WIN_COLOR_FG );

        fl_message_size_ = DEF_WIDGET_FSZ;
        fl_message_window_color_ = DEF_WIN_COLOR_BG;
        fl_message_label_color_ = DEF_WIN_COLOR_FG;
        fl_message_button_color_[ 0 ] = DEF_WIN_COLOR_BG;
        fl_message_button_color_[ 1 ] = DEF_WIN_COLOR_BG;
        fl_message_button_label_color_[ 0 ] = DEF_WIN_COLOR_FG;
        fl_message_button_label_color_[ 1 ] = DEF_WIN_COLOR_FG;
 
        // continue to child components ...
        window->begin();

        grpMain = new Fl_Group( 0, 0, ww, wh );
        if ( grpMain != NULL )
        {
            grpMain->begin();
        }

        // make transparency logo on back.
        imgLogo = createResImage( "simg_icon" );
        if ( imgLogo != NULL )
        {
            int iw = imgLogo->w();
            int ih = imgLogo->h();

            if ( ih > window->h() )
            {
                float dsf = float( ih ) / float( window->h() );

                iw /= dsf;
                ih /= dsf;

                Fl_RGB_Image* \
                imgPut = fl_imgtk::rescale( imgLogo, iw, ih, 
                                            fl_imgtk::BILINEAR );
                if ( imgPut != NULL )
                {
                    fl_imgtk::discard_user_rgb_image( imgLogo );
                    imgLogo = imgPut;
                }
            }

            fl_imgtk::blurredimage_ex( imgLogo, 3 );
            fl_imgtk::applyalpha_ex( imgLogo, 0.3f );

            boxLogo = new Fl_Box( 0, 0, iw, ih );
            if ( boxLogo != NULL )
            {
                boxLogo->box( FL_NO_BOX );
                boxLogo->image( imgLogo );
            }
        }
        else
        {
            boxLogo = NULL;
        }

        int putx = 100;
        int puty = 5;
        int puth = 25;

        outCapPath = new Fl_Output( putx, puty, 450, puth );
        if ( outCapPath != NULL )
        {
            APPLY_THEME_OUT( outCapPath );
            outCapPath->label( langset->Get( LangSet::BASE_PATH ) );
            outCapPath->value( _wc2utf8( pathSave.c_str() ) );
            putx += 450 + 5;
        }

        btnBrowsePath = new Fl_Button( putx, puty, 35, puth, "..." );
        if ( btnBrowsePath != NULL )
        {
            APPLY_THEME_BTN( btnBrowsePath );
            putx = 100;
            puty += puth;
        }

        boxCapAsmPath = new Fl_Box( putx, puty, 495, puth );
        if ( boxCapAsmPath != NULL )
        {
            APPLY_THEME_BOX( boxCapAsmPath );
            boxCapAsmPath->label( langset->Get( LangSet::BASE_PATH_INFO ) );
            boxCapAsmPath->labelcolor( 0xFF663300 );

            puty += puth;
        }

        outCapKey = new Fl_Output( putx, puty, 300, puth );
        if ( outCapKey != NULL )
        {
            APPLY_THEME_OUT( outCapKey );
            outCapKey->label( langset->Get( LangSet::CAPTURE_KEY ) );

            if ( cfgLdr->GetCapKey() == 0 )
            {
                // default capture key is F12.
                cfgLdr->SetCapKey( VK_F12 );
            }

            updateCapKey();
            
            putx += 300 + 5;
        }

        btnCapKey = new Fl_Button( putx, puty, 190, puth );
        if ( btnCapKey != NULL )
        {
            APPLY_THEME_BTN( btnCapKey );
            btnCapKey->label( langset->Get( LangSet::SELECT_KEY ) );

            putx = 100;
            puty += puth + 5;
        }

        int putw = window->w() - 105;
        putx = 100;

        btnOption = new Fl_Button( putx, puty, putw, puth );
        if ( btnOption != NULL )
        {
            APPLY_THEME_BTN( btnOption );
            btnOption->label( langset->Get( LangSet::OPTION ) );
            puty += puth + 5;
        }

        putx = 5;

        boxLastStatus = new Fl_Box( putx, 
                                    window->h() - puth - 5, 
                                    window->w() - 10,
                                    puth );
        if ( boxLastStatus != NULL )
        {
            boxLastStatus->color( fl_darker( window->color() ) );
            boxLastStatus->box( FL_THIN_UP_BOX );
            boxLastStatus->align( FL_ALIGN_INSIDE |
                                  FL_ALIGN_TOP_LEFT );
            boxLastStatus->labelfont( FL_COURIER );
            boxLastStatus->labelcolor( 0x3399CC00 );
            boxLastStatus->labelsize( window->labelsize() );

            laststatus = "(C)Copyright 2023, Raphael Kim, rageworx@@gmail.com";
            boxLastStatus->label( laststatus.c_str() );
        }

        if ( grpMain != NULL )
        {
            grpMain->end();
        }

        grpOption = new Fl_Group( 0, 0, ww, wh );
        if ( grpOption != NULL )
        {
            grpOption->begin();
        }

        boxOptionBg = new Fl_Box( 0, 0, ww, wh );
        if ( boxOptionBg != NULL )
        {
            boxOptionBg->box( FL_FLAT_BOX );
        }

        putx = 5;
        puty = 5;
        putw = ww-10;
        puth = window->labelsize() * 2;

        boxOptionTitle = new Fl_Box( putx, puty, putw, puth );
        if ( boxOptionTitle != NULL )
        {
            boxOptionTitle->box( FL_NO_BOX );
            boxOptionTitle->align( FL_ALIGN_LEFT | FL_ALIGN_INSIDE );
            boxOptionTitle->labelfont( window->labelfont() );
            boxOptionTitle->labelsize( window->labelsize() * 2 );
            boxOptionTitle->labelcolor( 0xFF663300 );
            boxOptionTitle->label( langset->Get( LangSet::OPTION ) );

            puty += boxOptionTitle->h() + 5;
        }

        boxOptionHR = new Fl_Box( putx, puty, putw, 2 );
        if ( boxOptionHR != NULL )
        {
            boxOptionHR->box( FL_THIN_UP_BOX );
            boxOptionHR->color( window->color() );
            puty += 2 + 5;
        }

        putw = 140;
        putx = 40;

        chkPrecisionCap = new Fl_Check_Button( putx, puty, putw, puth );
        if ( chkPrecisionCap != NULL )
        {
            APPLY_THEME_BTN( chkPrecisionCap );
            chkPrecisionCap->label( langset->Get( LangSet::PRECISION_CAPTURE ) );
            chkPrecisionCap->value( cfgLdr->GetOptPrecise() );

            putx += putw;
        }

        chkClientOnly = new Fl_Check_Button( putx, puty, putw, puth );
        if ( chkClientOnly != NULL )
        {
            APPLY_THEME_BTN( chkClientOnly );
            chkClientOnly->label( langset->Get( LangSet::CLIENT_AREA_ONLY ) );
            chkClientOnly->value( cfgLdr->GetOptClientOnly() );

            putx += putw;
        }

        chkPlaySound = new Fl_Check_Button( putx, puty, putw, puth );
        if ( chkPlaySound != NULL )
        {
            APPLY_THEME_BTN( chkPlaySound );
            chkPlaySound->label( langset->Get( LangSet::PLAY_CAPTURE_SOUND ) );
            chkPlaySound->value( cfgLdr->GetOptPlaySound() );

            putx += putw;
        }

        chkCopyClipboard = new Fl_Check_Button( putx, puty, putw, puth );
        if ( chkCopyClipboard != NULL )
        {
            APPLY_THEME_BTN( chkCopyClipboard );
            chkCopyClipboard->label( langset->Get( LangSet::COPY_TO_CLIPBOARD ) );
            chkCopyClipboard->value( cfgLdr->GetOptClipboard() );

            putx  = 40;
            puty += puth;
            putw  = ww - putx - 5;
        }

        chkUseNotifier = new Fl_Check_Button( putx, puty, putw, puth );
        if ( chkUseNotifier != NULL )
        {
            APPLY_THEME_BTN( chkUseNotifier );
            chkUseNotifier->label( langset->Get( LangSet::NOTIFIER ) );
            chkUseNotifier->value( cfgLdr->GetOptNotify() );

            puty += puth + 5;
            putx = 5;
        }

        putx = 150;
        putw = ww - putx - 5;

        chcCaptureType = new Fl_Choice( putx, puty, putw, puth );
        if ( chcCaptureType != NULL )
        {
            APPLY_THEME_CHOICE( chcCaptureType );
            chcCaptureType->label( langset->Get( LangSet::CAPTURE_TYPE ) );

            chcCaptureType->add( langset->Get( LangSet::CAPTYPE_WINDOW_RECT ) );
 
            chcCaptureType->add( langset->Get( LangSet::CAPTYPE_WINDOW_ONLY ) );

            chcCaptureType->value( cfgLdr->GetOptCaptureType() );

            puty += puth + 5;
        }

        putw /= 3;

        chcImageType = new Fl_Choice( putx, puty, putw, puth );
        if ( chcImageType != NULL )
        {
            APPLY_THEME_CHOICE( chcImageType );
            chcImageType->label( langset->Get( LangSet::IMAGE_TYPE ) );
            chcImageType->when( FL_WHEN_CHANGED );

            chcImageType->add( "image -- PNG" );
            chcImageType->add( "image -- JPEG" );

            putx += putw + 75;
            putw *= 1.5f;
        }

        sldImageQuality = new Fl_Hor_Fill_Slider( putx, puty, putw, puth );
        if ( sldImageQuality != NULL )
        {
            APPLY_THEME_SLIDER( sldImageQuality );

            putx = 5;
            puty += puth;
        }

        // update image type, and quality.
        if ( chcImageType != NULL )
        {
            chcImageType->value( cfgLdr->GetOptWriteType() );
            WidgetCall( chcImageType );

            if ( sldImageQuality != NULL )
            {
                sldImageQuality->value( cfgLdr->GetOptWriteQuality() );
            }

            updateimagequality();
        }

        if ( grpOption != NULL )
        {
            grpOption->end();
            grpOption->hide();
        }

        window->end();
        window->callback( WMain::WidgetCB, this );

        int wx = 0;
        int wy = 0;
        int ww = window->w();
        int wh = window->h();
        int dt = 0;

        cfgLdr->GetWindowPos( wx, wy, dt );

        int maxdt = Fl::screen_count();

        if ( dt > maxdt )
        {
            // seems screen changed.
            dt = maxdt;
            // reset to position 20,20.
            wx = 20;
            wy = 20;
        }
       
        window->resize( wx, wy, ww, wh );
        window->show();
    }

    ww = 300;
    wh = 50;

    winUtil = new Fl_Window( ww, wh, wintitlestr );
    if ( winUtil != NULL )
    {
        winUtil->color( DEF_WIN_COLOR_BG );
        winUtil->labelfont( DEF_WIDGET_FNT );
        winUtil->labelsize( DEF_WIDGET_FSZ );
        winUtil->labelcolor( DEF_WIN_COLOR_FG );
        winUtil->border( 0 );
        winUtil->begin();

        boxUtilBorder = new Fl_Box( 0,0, winUtil->w(), winUtil->h() );
        if ( boxUtilBorder != NULL )
        {
            APPLY_THEME_BOX2( boxUtilBorder );
            boxUtilBorder->box( FL_THIN_UP_BOX );
        }

        boxUtilIcon = new Fl_Box( 0,0, 50, 50, "?" );
        if ( boxUtilIcon != NULL )
        {
            APPLY_THEME_BOX2( boxUtilIcon );

            boxUtilIcon->box( FL_NO_BOX );
            boxUtilIcon->labelsize( boxUtilIcon->labelsize() * 3 );
        }

        boxUtilMsg = new Fl_Box( 55, 0, 245, 20 );
        if ( boxUtilMsg != NULL )
        {
            APPLY_THEME_BOX2( boxUtilMsg );
            static char strUtilMsg[128] = {0};
            sprintf( strUtilMsg, 
                     langset->Get( LangSet::QUIT_PROGRAM ),
                     "FLSCRNSHOT" );
            boxUtilMsg->label( strUtilMsg );
        }

        btnUtilYes = new Fl_Button( 55, 23, 110, 20 );
        if ( btnUtilYes != NULL )
        {
            APPLY_THEME_BTN2( btnUtilYes );
            btnUtilYes->label( langset->Get( LangSet::YES ) );
        }

        btnUtilNo = new Fl_Button( 170, 23, 110, 20 );
        if ( btnUtilNo != NULL )
        {
            APPLY_THEME_BTN2( btnUtilNo );
            btnUtilNo->label( langset->Get( LangSet::NO ) );
        }

        winUtil->end();
        winUtil->callback( WMain::WidgetCB, this );
        winUtil->hide();
    }

    if ( cfgLdr->GetWindowState() == 0 )
    {
        showhideWindow();
    }
}

bool WMain::captureWindow()
{
    bool opt_precCap = false;
    bool opt_clientOnly = false;
    bool opt_clipboard = false;
    int  opt_fmt = chcImageType->value();

    if ( chkPrecisionCap->value() > 0 )
        opt_precCap = true;

    if ( chkClientOnly->value() > 0 )
        opt_clientOnly = true;

    if ( chkCopyClipboard->value() > 0 )
        opt_clipboard = true;

    // capture !
    HWND hwndTarget = findfrontwindow();
    if ( hwndTarget != NULL )
    {
        wchar_t winclass[32] = {0};
        wchar_t wintext[32] = {0};
        WINDOWINFO winfo = {0};
        winfo.cbSize = sizeof( WINDOWINFO );

        GetWindowInfo( hwndTarget, &winfo );
        GetClassName( hwndTarget, winclass, 32 );
        GetWindowText( hwndTarget, wintext, 32 );

        if ( wcslen( winclass ) == 0 )
        {
            wcscpy( winclass, L"UnknownWindow" );
        }

        if ( wcslen( wintext ) == 0 )
        {
            wcscpy( wintext, L"Noname" );
        }
        else
        {
            // make safe name ...
            for( uint32_t cnt=0; cnt<wcslen( wintext ); cnt++ )
            {
                if ( ( wintext[cnt] == L'.' ) ||
                     ( wintext[cnt] == L'\\' ) ||
                     ( wintext[cnt] == L'/' ) ||
                     ( wintext[cnt] == L':' ) ||
                     ( wintext[cnt] == L'*' ) ||
                     ( wintext[cnt] == L'?' ) ||
                     ( wintext[cnt] == L'"' ) ||
                     ( wintext[cnt] == L'<' ) ||
                     ( wintext[cnt] == L'>' ) )
                {
                    wintext[cnt] = L'_';
                }
            }
        }

        // Get Whole screen.
        HDC hDC = NULL;

        int ct = cfgLdr->GetOptCaptureType();

        switch( ct )
        {
            default:
            case 0: /// screen to window area crop
                hDC = GetDC( NULL );
                break;

            case 1: /// window area.
                hDC = GetDC( hwndTarget );
                break;
        }

        if ( hDC != NULL )
        {
            HDC hWDC = CreateCompatibleDC( hDC );

            if ( hWDC == NULL )
                return false;

            int sx     = winfo.rcWindow.left;
            int sy     = winfo.rcWindow.top;
            int width  = winfo.rcWindow.right - winfo.rcWindow.left;
            int height = winfo.rcWindow.bottom - winfo.rcWindow.top;

            if ( ct == 1 )
            {
                sx = 0;
                sy = 0;

                width  = winfo.rcClient.right - winfo.rcClient.left;
                height = winfo.rcClient.bottom - winfo.rcClient.top;
                if ( dpiR > 1.f )
                {
                    width *= dpiR;
                    height *= dpiR;
                }
            }
            else
            {
                if ( opt_precCap == true )
                {
                    sx     += winfo.cxWindowBorders;
                    width  -= winfo.cxWindowBorders * 2;
                    height -= winfo.cyWindowBorders;
                }

                if ( opt_clientOnly == true )
                {
                    sx     = winfo.rcClient.left;
                    sy     = winfo.rcClient.top;

                    width  = winfo.rcClient.right - winfo.rcClient.left;
                    height = winfo.rcClient.bottom - winfo.rcClient.top;
                }
                
                if ( dpiR > 1.f )
                {
                    sx *= dpiR;
                    sy *= dpiR;
                    width *= dpiR;
                    height *= dpiR;
                }
            }

            HBITMAP hBmp = CreateCompatibleBitmap( hDC,
                                                   width,
                                                   height );

            // hBmp == copying bitmap.
            HGDIOBJ hPrevObj = SelectObject( hWDC, hBmp );

            int ret = \
            BitBlt( hWDC,
                    0, 
                    0, 
                    width, 
                    height, 
                    hDC,
                    sx, 
                    sy, 
                    SRCCOPY | CAPTUREBLT );
 
            SelectObject( hWDC, hPrevObj );

            if ( opt_clipboard == true )
            {
                BitmapToClipboard( hBmp );
            }

            // Convert HBMITMAP to Fl_RGB_Image
            Fl_RGB_Image* img = to_flrgb( hWDC, hBmp );

            if ( img != NULL )
            {
                // check directory exsist,
                wchar_t writepath[250] = {0};
                swprintf( writepath, L"%S\\%S[%S]", 
                          pathSave.c_str(), 
                          winclass,
                          wintext );

                bool writable = true;

                if ( testDirectory( writepath ) == false )
                {
                    if ( createDirectory( writepath ) == false )
                    {
                        WRITESTATUS( "ERROR: "
                                     "BasePath cannot accessabled." );
                        writable = false;
                    }
                }

                if ( writable == true )
                {
                    // Make write path ...
                    time_t rtime;
                    time( &rtime );
                    struct tm* tinfo = localtime( &rtime );
                    uint16_t ms = GetTickCount() % 1000;

                    wchar_t writemap[512] = {0};
                    wchar_t fext[5] = {0};

                    switch ( opt_fmt )
                    {
                        default:
                        case 0 :/// PNG
                            wcscpy( fext, L"png" );
                            break;
    
                        case 1: /// JPEG
                            wcscpy( fext, L"jpg" );
                            break;
                    }

                    swprintf( writemap, 
                              L"%S\\%04d-%02d-%02d-%02d-%02d-%02d-%03u.%S",
                              writepath,
                              tinfo->tm_year + 1900,
                              tinfo->tm_mon + 1,
                              tinfo->tm_mday,
                              tinfo->tm_hour,
                              tinfo->tm_min,
                              tinfo->tm_sec,
                              ms,
                              fext);

                    // Save to PNG ...
                    int qual = cfgLdr->GetOptWriteQuality();
                    bool retb = false;

                    switch( opt_fmt )
                    {
                        default:
                        case 0: /// PNG
                            retb = save2png( img, writemap, qual );
                            break;

                        case 1: /// JPEG
                            retb = save2jpg( img, writemap, qual );
                            break;
                    }

                    if ( retb == false )
                    {
                        WRITESTATUS( "ERROR:"
                                     "Cannot write image file !" );
                    }
                    else
                    {
                        lastwritten = writemap;
                    }
                }

                delete img;
            }

            DeleteObject( hBmp );
            DeleteDC( hWDC );
            DeleteDC( hDC );

            WRITESTATUS( "OK: Successfully captured" );

            return true;
        }
    }
    else
    {
        WRITESTATUS( "ERROR:"
                     "Cannot recognized current window" );
    }

    return false;
}

bool WMain::checkWritePath()
{
    if ( testDirectory( pathSave.c_str() ) == false )
    {
        if ( createDirectory( pathSave.c_str() ) == false )
        {
            return false;
        }
    }

    cfgLdr->SetLastPath( pathSave.c_str() );
    cfgLdr->Save();

    return true;
}

void WMain::selectNewPath()
{
    btnBrowsePath->deactivate();

    char utf8path[512] = {0};

    fl_utf8fromwc( utf8path, 512, pathSave.c_str(), pathSave.size() );

    Fl_Native_File_Chooser nfc;
    nfc.title( langset->Get( LangSet::SELECT_NEW_DIR ) );
    nfc.type( Fl_Native_File_Chooser::BROWSE_DIRECTORY | 
              Fl_Native_File_Chooser::NEW_FOLDER );
    nfc.directory( utf8path );

    int ret = nfc.show();

#ifdef DEBUG
    printf( "nfc.show() = %d\n", ret );
    fflush( stdout );
#endif

    if ( ret == 0 )
    {
        const char* refdir = nfc.filename(0);
#ifdef DEBUG
        printf( "nfc.directory() = %s\n", refdir );
        fflush( stdout );
#endif
        if ( strlen( refdir ) > 0 )
        {
            wchar_t testdir[512] = {0};
            fl_utf8towc( refdir, strlen( refdir ), testdir, 512 );
            pathSave = testdir;

            cfgLdr->SetLastPath(  pathSave.c_str() );
            cfgLdr->Save();

            const char* utf8path = _wc2utf8( pathSave.c_str() );
            
            outCapPath->value( utf8path );
            outCapPath->redraw();
        }
    }

    btnBrowsePath->activate();
}
        
void WMain::reloadConfigs()
{
    cfgLdr->Save();

    if ( cfgLdr->Reload() == true )
    {
        chkPrecisionCap->value( cfgLdr->GetOptPrecise() );
        chkClientOnly->value( cfgLdr->GetOptClientOnly() );
        chkCopyClipboard->value( cfgLdr->GetOptClipboard() );
        chkPlaySound->value( cfgLdr->GetOptPlaySound() );

        pathSave = cfgLdr->GetLastPath();
        outCapPath->value( _wc2utf8( pathSave.c_str() ) );
    }
}

void WMain::playCaptureSound()
{
    if ( ( sndBuffer != NULL ) && ( cfgLdr->GetOptPlaySound() > 0 ) )
    {
        sndPlaySound( NULL, SND_ASYNC );
        sndPlaySound( (LPCWSTR)sndBuffer, 
                      SND_MEMORY | SND_SYNC | SND_NODEFAULT );
    }
}

void WMain::notifying()
{
    if ( notifier != NULL )
    {
        static wstring msg;

        msg = L"Successfully captured screenshot to\n";
        
        if ( lastwritten.size() > 0 )
        {
            msg += stripFileName( lastwritten.c_str() );
        }
        else
        {
            msg += L"an image file.";
        }

        notifier->pushMsg( L"FLScrnshot captured :", 
                           msg.c_str(),
                           2000 );
    }
}

void WMain::showhideWindow()
{
    HWND hME = fl_xid( window );
    LONG wstyle = GetWindowLong( hME, GWL_STYLE );

    // FLTK is WS_EX_TOOLWINDOW, not APPWINDOW.

    if ( ( wstyle & WS_VISIBLE ) == 0 )
    {
        ShowWindow( hME, SW_MINIMIZE );
        wstyle |= WS_VISIBLE;
        SetWindowLong( hME, GWL_STYLE, wstyle );
        ShowWindow( hME, SW_RESTORE );
        SetActiveWindow( hME );
        SetForegroundWindow( hME );
        window->activate();
        window->redraw();
        winvisstate = 1;
   }
    else
    {
        ShowWindow( hME, SW_HIDE );
        wstyle &= ~( WS_VISIBLE );
        SetWindowLong( hME, GWL_STYLE, wstyle );
        ShowWindow( hME, SW_HIDE );
        winvisstate = 0;
    }

    // Write current windows state ...
    if ( cfgLdr != NULL )
    {
        cfgLdr->SetWindowState( winvisstate );
        cfgLdr->Save();
    }
}

void WMain::registerPopMenu()
{
    popMenu = new Fl_Menu_Button( 0, 0, window->w(), window->h() );
    if ( popMenu != NULL )
    {
        popMenu->type( Fl_Menu_Button::POPUP3 );
        popMenu->color( window->color(), window->labelcolor() );
        popMenu->selection_color( window->color() );
        popMenu->labelfont( window->labelfont() );
        popMenu->textfont( window->labelfont() );
        popMenu->labelsize( window->labelsize() );
        popMenu->textsize( window->labelsize() );
        popMenu->labelcolor( window->labelcolor() );
        popMenu->textcolor( window->labelcolor() );

        int ckey[] = {
            FL_CTRL + 'h',
            FL_CTRL + 'r',
            FL_CTRL + 'q',
            0x20 };

        popMenu->add( "Show or Hide\t", ckey[0], WMain::WidgetCB, this, 
                      0 );
        popMenu->add( "Reload      \t", ckey[1], WMain::WidgetCB, this,
                      FL_MENU_DIVIDER);
        popMenu->add( "Quit        \t", ckey[2], WMain::WidgetCB, this, 
                      0 );

        // Ok. then, insert popup to main window.
        window->add( popMenu );
    }
}

void WMain::updateimagequality()
{
    static char striq[32] = {0};
    double q = sldImageQuality->value();

    sprintf( striq, "Q.[%03d] :", (int) q );
    sldImageQuality->label( striq );
    sldImageQuality->redraw();
}

void WMain::updateconfigs()
{
    // store changes to cfgldr.
    //
    cfgLdr->SetOptPrecise( chkPrecisionCap->value() );
    cfgLdr->SetOptClientOnly( chkClientOnly->value() );
    cfgLdr->SetOptClipboard( chkCopyClipboard->value() );
    cfgLdr->SetOptPlaySound( chkPlaySound->value() );
    cfgLdr->SetOptNotify( chkUseNotifier->value() );
    cfgLdr->SetOptCaptureType( chcCaptureType->value() );
    cfgLdr->SetOptWriteType( chcImageType->value() );
    cfgLdr->SetOptWriteQuality( sldImageQuality->value() );
}

bool WMain::save2png( Fl_RGB_Image* img, const wchar_t* fname, int q )
{
    if ( ( img != NULL ) && ( fname != NULL ) )
    {
        if ( img->d() < 3 )
            return false;

        if ( _waccess( fname, F_OK ) == 0 )
        {
            if ( _wunlink( fname ) != 0 )
                return false; /// failed to remove file !
        }

        FILE* fp = _wfopen( fname, L"wb" );
        if ( fp != NULL )
        {
            png_structp png_ptr     = NULL;
            png_infop   info_ptr    = NULL;
            png_bytep   row         = NULL;

            png_ptr = png_create_write_struct( PNG_LIBPNG_VER_STRING,
                                               NULL,
                                               NULL,
                                               NULL );
            if ( png_ptr != NULL )
            {
                info_ptr = png_create_info_struct( png_ptr );
                if ( info_ptr != NULL )
                {
                    if ( setjmp( png_jmpbuf( (png_ptr) ) ) == 0 )
                    {
                        char tmpstr[1024] = {0,};

                        int png_c_type = PNG_COLOR_TYPE_RGB;

                        if ( img->d() == 4 )
                        {
                            png_c_type = PNG_COLOR_TYPE_RGBA;
                        }

                        png_init_io( png_ptr, fp );
                        // Let compress image with libz
                        //  changing compress levle 8 to 7 for speed.
                        png_set_compression_level( png_ptr, q );
                        png_set_IHDR( png_ptr,
                                      info_ptr,
                                      img->w(),
                                      img->h(),
                                      8,
                                      png_c_type,
                                      PNG_INTERLACE_NONE,
                                      PNG_COMPRESSION_TYPE_BASE,
                                      PNG_FILTER_TYPE_BASE);

                        png_write_info( png_ptr, info_ptr );

                        const uchar* buff = (const uchar*)img->data()[0];

                        uint32_t rowsz = img->w() * sizeof( png_byte ) * img->d();

                        row = (png_bytep)malloc( rowsz );
                        if ( row != NULL )
                        {
                            int bque = 0;
                            int minur = img->h() / 50;

                            for( int y=0; y<img->h(); y++ )
                            {
                                memcpy( row, &buff[bque], rowsz );
                                bque += rowsz;

                                png_write_row( png_ptr, row );

                                // some progress ..
                                char strprog[100] = "Writing PNG:";
                                uint32_t sp = strlen( strprog );
                                float progf = (float)y / (float)img->h();
                                uint32_t tg = (uint32_t)(60.f * progf);
                                for( uint32_t cnt=0; cnt<tg; cnt++ )
                                {
                                    strprog[sp+cnt] = '>';
                                }
                                
                                // just update string inside, then redraw.
                                laststatus = strprog;
                                if ( grpMain->active_r() > 0 )
                                    boxLastStatus->redraw();
                           }

                            png_write_end( png_ptr, NULL );

                            fclose( fp );
                            free(row);

                            window->label( wintitlestr );
                        }

                        png_free_data(png_ptr, info_ptr, PNG_FREE_ALL, -1);
                        png_destroy_write_struct(&png_ptr, (png_infopp)NULL);

                        return true;
                    }
                    png_free_data(png_ptr, info_ptr, PNG_FREE_ALL, -1);
                }
                png_destroy_write_struct(&png_ptr, (png_infopp)NULL);
            }
        }
    }

    return false;
}

bool WMain::save2jpg( Fl_RGB_Image* img, const wchar_t* fname, int q )
{
    bool retb = false;

    if ( ( img != NULL ) && ( fname != NULL ) )
    {
        if ( img->d() == 2 ) /// RGB565,555 not supported.
            return false;

        int w = img->w();
        int h = img->h();
        int d = img->d();
        int jfmt = 0;
        int jsmp = 0;

        tjhandle jComp = tjInitCompress();

        if ( jComp != 0 )
        {
            switch( d )
            {
                case 1: /// gray.
                    jfmt = TJPF_GRAY;
                    jsmp = TJSAMP_GRAY;
                    break;

                case 3: /// RGB
                    jfmt = TJPF_RGB;
                    jsmp = TJSAMP_444;
                    break;

                case 4: /// RGBA
                    jfmt = TJPF_RGBA;
                    jsmp = TJSAMP_444;
                    break;
            }

            const uint8_t* refa = \
                (const uint8_t*)img->data()[0];
            
            uint8_t*           outb = NULL;
            long unsigned int  outsz = 0;

            tjCompress2( jComp, refa, w, 0, h, jfmt,
                         &outb, &outsz, jsmp, q,
                         TJFLAG_FASTDCT );
            tjDestroy( jComp );

            if ( ( outb != NULL ) && ( outsz > 0 ) )
            {
                FILE* fp = _wfopen( fname, L"wb" );
                if ( fp != NULL )
                {
                    fwrite( outb, 1, outsz, fp );
                    fclose( fp );

                    retb = true;
                }

                delete[] outb;
                outsz = 0;
            }
        }
    }

    return retb;
}

void WMain::removeOptionBg()
{
    if ( imgOptionBg != NULL )
    {
        boxOptionBg->image( NULL );
        fl_imgtk::discard_user_rgb_image( imgOptionBg );
    }
}

bool WMain::createThread( uint32_t idx )
{
    if ( idx < MAX_THREADS_CNT )
    {
        //killThread( idx );
        if ( threads[idx] != NULL ) /// it means still in process...
            return false;

        threads[idx] = new ThreadParam;
        if ( threads[idx] != NULL )
        {
            threads[idx]->paramL = idx;
            threads[idx]->paramData = this;

            pthread_create( &threads[idx]->ptt,
                            NULL, WMain::PThreadCB,
                            threads[idx] );

            return true;
        }
    }
    return false;
}

void WMain::killThread( uint32_t idx )
{
    if ( idx == THREAD_KILL_ALL )
    {
        for( uint32_t cnt=0; cnt<MAX_THREADS_CNT; cnt++ )
        {
            killThread( cnt );
        }

        return;
    }
    else
    if ( idx < MAX_THREADS_CNT )
    {
        if ( threads[idx] != NULL )
        {
            pthread_t ptt = threads[idx]->ptt;
#ifdef _WIN32
            // godam windows only sens it is warning.
            if ( ptt != 0 )
#else
            if ( ptt != NULL )
#endif /// of _WIN32
            {
                pthread_kill( ptt, 0 );
                pthread_join( ptt, NULL );
            }

            delete threads[idx];
            threads[idx] = NULL;
        }
    }
}
