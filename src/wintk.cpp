#include "wintk.h"

#define MIN_HANDLEVALUE     32

const wchar_t* findclassfromhandle( HWND h )
{
    static wchar_t clsnm[64] = {0};

    return clsnm;
}

HWND findfrontwindow()
{
    return GetForegroundWindow();
}

HWND findwindowbyname( HWND htarget )
{
    const wchar_t* clsnm = findclassfromhandle( htarget );
    if ( clsnm != NULL )
    {
        return FindWindow( clsnm, NULL );
    }

    return NULL;
}

HWND findwindow( const wchar_t* title, bool exactly = false )
{
    return NULL;
}

HDC  getwindowDC( HWND htarget )
{
    return GetDC( htarget );    
}
