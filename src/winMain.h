#ifndef __WINMAIN_H__
#define __WINMAIN_H__

#include <FL/Fl.H>
#include <FL/Fl_Window.H>
#include <FL/Fl_Double_Window.H>
#include <FL/Fl_Box.H>
#include <FL/Fl_Image.H>
#include <FL/Fl_RGB_Image.H>
#include <FL/Fl_Choice.H>
#include <FL/Fl_Button.H>
#include <FL/Fl_Input.H>
#include <FL/Fl_Output.H>
#include <FL/Fl_Menu_.H>
#include <FL/Fl_Menu_Item.H>
#include <FL/Fl_Menu_Button.H>
#include <FL/Fl_Check_Button.H>
#include <FL/Fl_Hor_Fill_Slider.H>

#include "Fl_Image_Button.h"

#include <pthread.h>

#include <vector>
#include <string>

#include "winkeyhook.h"
#include "winnotify.h"
#include "langset.h"
#include "cfgldr.h"

class WMain : public WinKeyHookEvent, public WinNotifyEvent
{
    typedef struct
        _ThreadParam{
            pthread_t   ptt;
            long        paramL;
            void*       paramData;
        }ThreadParam;

    public:
        WMain( int argc, char** argv, const char* loc );
        ~WMain();

    public:
        int Run();

    public:
        void* PThreadCall( ThreadParam* tparam );
        static void* PThreadCB( void* p )
        {
            if ( p != NULL )
            {
                ThreadParam* tp = (ThreadParam*)p;
                WMain* pwm = (WMain*)tp->paramData;
                return pwm->PThreadCall(tp);
            }
            return NULL;
        }
        void WidgetCall( Fl_Widget* w );
        static void WidgetCB( Fl_Widget* w, void* p )
        {
            if ( p != NULL )
            {
                WMain* pwm = (WMain*)p;
                pwm->WidgetCall( w );
            }
        }

    public:
        void OnKeyEvent( unsigned tp, unsigned wp, unsigned lp );
        void OnNotiEvent( unsigned tp, unsigned wp, unsigned lp );

    private:
        void setdefaultwintitle();
        void resetParameters();
        void createComponents();
        void registerPopMenu();
        void getEnvironments();
        void updateCapKey();
        bool captureWindow();
        bool checkWritePath();
        void selectNewPath();
        void reloadConfigs();
        void playCaptureSound();
        void notifying();
        void showhideWindow();
        void updateimagequality();
        void updateconfigs();
        bool save2png( Fl_RGB_Image* img, const wchar_t* fname, int q = 7 );
        bool save2jpg( Fl_RGB_Image* img, const wchar_t* fname, int q = 81 );
        void removeOptionBg();
        bool createThread( unsigned idx );
        void killThread( unsigned idx );

    private:
        int     _argc;
        char**  _argv;

    private:
        float       crossfade_ratio;
        unsigned    crossfade_idx;
        bool        timermutex;
        bool        busyflag;
        bool        keycapflag;
        bool        poputilflag;
        int         poputilret;
        unsigned    lastkey;
        bool        goingquit;
        int         winvisstate;

    protected:
        Fl_Double_Window*   window;
        Fl_Group*           grpMain;
        Fl_Box*             boxLogo;
        Fl_Menu_Button*     popMenu;
        Fl_Output*          outCapPath;
        Fl_Output*          outCapKey;
        Fl_Box*             boxCapAsmPath;
        Fl_Button*          btnBrowsePath;
        Fl_Button*          btnCapKey;
        Fl_Button*          btnOption;
        Fl_Box*             boxLastStatus;
        Fl_RGB_Image*       imgLogo;
        Fl_Group*           grpOption;
        Fl_Box*             boxOptionBg;
        Fl_RGB_Image*       imgOptionBg;
        Fl_Box*             boxOptionTitle;
        Fl_Box*             boxOptionHR;
        Fl_Check_Button*    chkPrecisionCap;
        Fl_Check_Button*    chkClientOnly;
        Fl_Check_Button*    chkCopyClipboard;
        Fl_Check_Button*    chkPlaySound;
        Fl_Check_Button*    chkUseNotifier;
        Fl_Choice*          chcCaptureType;
        Fl_Choice*          chcImageType;
        Fl_Hor_Fill_Slider* sldImageQuality;

    protected:
        Fl_Window*          winUtil;
        Fl_Box*             boxUtilBorder;
        Fl_Box*             boxUtilIcon;
        Fl_Box*             boxUtilMsg;
        Fl_Button*          btnUtilYes;
        Fl_Button*          btnUtilNo;

    protected:
        unsigned char*      sndBuffer;
        unsigned            sndBuffersz;

    protected:
        HICON               hIconWindowLarge;
        HICON               hIconWindowSmall;
        HICON               hIconNotifier;

    private:
        #define             MAX_WINTITLE_LEN        512
        char wintitlestr[MAX_WINTITLE_LEN];

    protected:
        std::wstring        pathHome;
        std::wstring        pathSystemBase;
        std::wstring        pathUserData;
        std::wstring        pathUserRoaming;

	protected:
        std::wstring        pathSave;
        std::wstring        lastwritten;
        std::string         laststatus;

    protected:
        #define             MAX_THREADS_CNT             1
        #define             THREAD_KILL_ALL             -1
        ThreadParam*        threads[MAX_THREADS_CNT];
        ConfigLoader*       cfgLdr;
        WinKeyHook*         keyhooker;
        WinNotify*          notifier;
        LangSet*            langset;
		float 				dpiR;
};

#endif // __WINMAIN_H__
