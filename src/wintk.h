#ifndef __WINTK_H__
#define __WINTK_H__

#include <windows.h>

HWND findfrontwindow();
HWND findwindow( HWND htarget );
HWND findwindowbyname( const wchar_t* title, bool exactly = false );
HDC  getwindowDC( HWND htarget );

#endif /// of __WINTK_H__ 
