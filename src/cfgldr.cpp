#include <unistd.h>
#include <windows.h>

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <string>

#include <FL/fl_utf8.h>
#include <minIni.h>
#include "cfgldr.h"

///////////////////////////////////////////////////////////////////////

using namespace std;

///////////////////////////////////////////////////////////////////////

#define CFGF_NAME   L"flscrnshot.cfg"
#define DEFAULT_INI "[WINDOW]\nX=20\nY=20\nWM=0\nDT=0\nWS=1\n"\
                    "[USER]\nusermode=0\ncapkey=\n"\
                    "precise=1\nclientonly=1\nclipboard=0\n"\
                    "playsound=1\nnotify=0\ncaptype=0\nlastpath=\n"\
                    "[IMAGE]\ntype=0\nquality=7\n"

///////////////////////////////////////////////////////////////////////

const char* Convert2MBSC( const wchar_t* str )
{
    if ( str == NULL )
        return NULL;

    char* pStr = NULL;

    int strSize = WideCharToMultiByte(CP_ACP, 0,str,-1, NULL, 0,NULL, 0);
    pStr = new char[strSize];
    if ( pStr != NULL )
    {
        WideCharToMultiByte(CP_ACP, 0, str, -1, pStr, strSize, 0,0);
    }

    return pStr;
}

const wchar_t* Convert2WCS( const char* str )
{
    if ( str == NULL )
        return NULL;

    wchar_t* pStr = NULL;

    int strSize = MultiByteToWideChar(CP_ACP, 0,str, -1, NULL, 0);
    pStr = new WCHAR[strSize];
    if ( pStr != NULL )
    {
        MultiByteToWideChar(CP_ACP, 0,str, strlen(str)+1, pStr, strSize);
    }

    return pStr;
}

void createdefaultini( wstring &fpath )
{
    FILE* fp = _wfopen( fpath.c_str(), L"wb" );
    if ( fp != NULL )
    {
        char refstr[] = DEFAULT_INI;
        fwrite( refstr, 1, strlen( refstr ), fp );
        fclose( fp );
    }
    else
    {
        printf( "Failed to create INI (%S)!\n", fpath.c_str() );
    }
}

///////////////////////////////////////////////////////////////////////

ConfigLoader::ConfigLoader( const char* argv0 )
    : pargv0( argv0 )
{
    clearall();
    getbasepath();
}

ConfigLoader::~ConfigLoader()
{
    Save();
}

bool ConfigLoader::Load()
{
    loaded = false;

    wstring loadpath = basepath;

    if ( loadpath.size() == 0 )
    {
        loadpath = L".";
    }

    // check directory first.
    if ( _waccess( loadpath.c_str(), 0 ) != 0 )
    {
        _wmkdir( loadpath.c_str() );
    }

    loadpath += L"\\";
    loadpath += CFGF_NAME;

    if ( _waccess( loadpath.c_str(), 0 ) != 0 )
    {
        createdefaultini( loadpath );
    }

#ifdef DEBUG
    printf( "Starting to load config in %S ...\n", loadpath.c_str() );
    fflush( stdout );
#endif

    const char* mbsc_loadpath = Convert2MBSC( loadpath.c_str() );

    minIni *pIni = new minIni( mbsc_loadpath );
    if ( pIni != NULL )
    {
        wx = pIni->geti( "WINDOW", "X", 10 );
        wy = pIni->geti( "WINDOW", "Y", 10 );
        dt = pIni->geti( "WINDOW", "DT", 0 );
        winstate = pIni->geti( "WINDOW", "WS", 1 );

        if ( winstate < 0 )
        {
            winstate = 0;
        }

        string tmpstr;

        usermode = pIni->geti( "USER", "usermode", 0 );
        capkey   = (unsigned)pIni->getl( "USER", "capkey", 0 );
        tmpstr   = pIni->gets( "USER", "lastpath" );
        if ( tmpstr.size() > 0 )
        {
            const wchar_t* convstr = Convert2WCS( tmpstr.c_str() );

            if ( convstr != NULL )
            {
                wcsncpy( lastpath, convstr, 512 );
#ifdef DEBUG
                printf( "lastpath loaded to %S\n", lastpath );
#endif /// of DEBUG                
            }
        }

        opt_precise     = pIni->geti( "USER", "precise", 1 );
        opt_clientonly  = pIni->geti( "USER", "clientonly", 1 );
        opt_clipboard   = pIni->geti( "USER", "clipboard", 0 );
        opt_playsound   = pIni->geti( "USER", "playsound", 1 );
        opt_notify      = pIni->geti( "USER", "notify", 0 );
        opt_captype     = pIni->geti( "USER", "captype", 0 );
        opt_writetype   = pIni->geti( "IMAGE", "type", 0 );
        opt_writequality= pIni->geti( "IMAGE", "quality", 7 );

        if ( usermode < 0 )
            usermode = 0;

        if ( capkey < 0 )
            capkey = 0;

        // check quality boundary.
        switch( opt_writetype )
        {
            case 0: /// PNG.
                if ( opt_writequality < 1 )
                {
                    opt_writequality = 1;
                }
                else
                if ( opt_writequality > 9 )
                {
                    opt_writequality = 9;
                }
                break;

            case 1: /// JPEG.
                if ( opt_writequality < 10 )
                {
                    opt_writequality = 10;
                }
                else
                if ( opt_writequality > 100 )
                {
                    opt_writequality = 100;
                }
                break;

            default:
                opt_writetype = 0;
                opt_writequality = 7;
                break;
        }

        delete pIni;

        loaded = true;
    }

    return loaded;
}

bool ConfigLoader::Reload()
{
    wstring tmpbp = basepath;
    clearall();
    wcsncpy( basepath, tmpbp.c_str(), 512 );
    return Load();
}

bool ConfigLoader::Save()
{
    if ( loaded == true )
    {
        wstring loadpath = basepath;

        if ( loadpath.size() == 0 )
        {
            loadpath = L".";
        }

        loadpath += L"\\";
        loadpath += CFGF_NAME;
        
        const char* mbsc_loadpath = Convert2MBSC( loadpath.c_str() );

        minIni *pIni = new minIni( mbsc_loadpath );
        if ( pIni != NULL )
        {
            pIni->put( "WINDOW", "X", wx );
            pIni->put( "WINDOW", "Y", wy );
            pIni->put( "WINDOW", "DT", dt );
            pIni->put( "WINDOW", "WS", winstate );

            pIni->put( "USER", "usermode", usermode );
            pIni->put( "USER", "capkey", (long)capkey );
            pIni->put( "USER", "precise", opt_precise );
            pIni->put( "USER", "clientonly", opt_clientonly );
            pIni->put( "USER", "clipboard", opt_clipboard );
            pIni->put( "USER", "playsound", opt_playsound );
            pIni->put( "USER", "notify", opt_notify );
            pIni->put( "USER", "captype", opt_captype );
            pIni->put( "USER", "lastpath", Convert2MBSC( lastpath ) );

            pIni->put( "IMAGE", "type", opt_writetype );
            pIni->put( "IMAGE", "quality", opt_writequality );

            delete pIni;

            return true;
        }
    }
    return false;
}

void ConfigLoader::GetWindowPos( int &x, int &y,  int &dt )
{
    x = wx;
    y = wy;
    dt = this->dt;
}

void ConfigLoader::SetWindowPos( int x, int y, int dt )
{
    wx = x;
    wy = y;
    this->dt = dt;
}

const wchar_t* ConfigLoader::GetLastPath()
{
    return lastpath;
}

void ConfigLoader::SetLastPath( const wchar_t* path )
{
    if ( path != NULL )
    {
        wcsncpy( lastpath, path, 512 );
    }
}

void ConfigLoader::clearall()
{
    usermode = 0;
    capkey = 0;
    memset( lastpath, 0, 512 * sizeof( wchar_t ) );
    memset( basepath, 0, 512 * sizeof( wchar_t ) );
}

void ConfigLoader::getbasepath()
{
    const wchar_t* ppath = _wgetenv( L"LOCALAPPDATA" );
    if ( ppath != NULL )
    {
        snwprintf( basepath, 512, L"%S\\FLSCRNSHOT", ppath );
    }
#ifdef DEBUG
    printf( "ConfigLoader, basepath = [%S]\n", basepath );
#endif
}
