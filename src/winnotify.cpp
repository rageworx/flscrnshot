#include "winnotify.h"
#include <cstdio>
#include <cstdlib>
#include <cstring>

//////////////////////////////////////////////////////////////////////////

#define DEF_WINNOTI_CLSNAME         L"flssnoti"
#define DEF_NOTI_UID                0x464C5353 // == "FLSS"
#define DEF_WM_NOTI                 (WM_APP+11)

//////////////////////////////////////////////////////////////////////////

static WinNotify* instNoti = NULL;

//////////////////////////////////////////////////////////////////////////

LRESULT CALLBACK hiddenWinHandler(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);

//////////////////////////////////////////////////////////////////////////

WinNotify::WinNotify( HINSTANCE hIparent, HWND hWparent, HICON hIcon,
                      WinNotifyEvent* e )
 : hInstParent( hIparent ),
   hWndParent( hWparent ),
   hHiddenWin( NULL ),
   hNotiIcon( hIcon ),
   evt( e )
{
    unsigned tipsz = ARRAYSIZE( notidata.szTip );

    memset( tipdata, 0, 128 * sizeof( wchar_t ) );

    GetWindowText( hWparent, tipdata, 128 );

    if ( createHiddenChildWindow() == true )
    {
        //register notifier ...
        memset( &notidata, 0, sizeof( NOTIFYICONDATA ) );

        notidata.cbSize = sizeof( NOTIFYICONDATA );
        notidata.hWnd   = hHiddenWin;
        notidata.uID    = DEF_NOTI_UID;
        notidata.hIcon  = hNotiIcon;
        notidata.uFlags = NIF_MESSAGE | NIF_ICON | NIF_SHOWTIP | NIF_TIP;
        notidata.uCallbackMessage = DEF_WM_NOTI;

        if ( wcslen( tipdata ) > 0 )
        {
            wcsncpy( notidata.szTip, tipdata, tipsz );
        }

        Shell_NotifyIcon( NIM_ADD, &notidata );
    }

    if ( instNoti != NULL )
    {
        delete instNoti;
    }

    instNoti = this;
}

WinNotify::~WinNotify()
{
    // unregister notifier.
    notidata.uFlags = NIF_MESSAGE | NIF_ICON | NIF_SHOWTIP | NIF_TIP;
    Shell_NotifyIcon( NIM_DELETE, &notidata );

    destroyHiddenChildWindow();

    instNoti = NULL;
}

void WinNotify::tooltip( const wchar_t* msg )
{
    if ( hHiddenWin != NULL )
    {
        if ( msg != NULL )
        {
            unsigned tipsz = ARRAYSIZE( notidata.szTip );

            wcsncpy( tipdata, msg, 128 );
            wcsncpy( notidata.szTip, tipdata, tipsz );
            notidata.dwInfoFlags = 0;
            notidata.uFlags = 0;

            Shell_NotifyIcon( NIM_MODIFY, &notidata );
        }
    }
}

void WinNotify::pushMsg( const wchar_t* title, const wchar_t* msg, unsigned ms )
{
    if ( title != NULL )
        wcsncpy( notidata.szInfoTitle, title, 255 );
    else
        notidata.szInfoTitle[0] = 0;

    if ( msg != NULL )
        wcsncpy( notidata.szInfo, msg, 255 );
    else
        notidata.szInfo[0] = 0;

    // resist abnormal termination !
    if ( ms < 1000 )
        ms = 1000;

    notidata.dwInfoFlags = NIIF_INFO;
    notidata.uFlags = NIF_INFO;
    notidata.uTimeout = ms;

    Shell_NotifyIcon( NIM_MODIFY, &notidata );
}

void WinNotify::MsgEvt( unsigned* p )
{
    if ( evt != NULL )
    {
        evt->OnNotiEvent( p[0], p[1], p[2] );
    }
#ifdef DEBUG
    else
    {
        printf( "ERROR: no event handler ???\n" );
        fflush( stdout );
    }
#endif
}

bool WinNotify::createHiddenChildWindow()
{
    if ( hHiddenWin == NULL )
    {
		WNDCLASSEX wc = {0};
		wc.cbSize = sizeof(wc);
		wc.lpfnWndProc = &hiddenWinHandler;
		wc.style = CS_HREDRAW | CS_VREDRAW;
		wc.cbWndExtra = 0;
		wc.cbClsExtra = 0;
		wc.hInstance = hInstParent;
		wc.hbrBackground = (HBRUSH) GetStockObject(NULL_BRUSH);
		wc.lpszMenuName = NULL;
		wc.lpszClassName = DEF_WINNOTI_CLSNAME;
		wc.hIcon = NULL;
		wc.hIconSm = wc.hIcon;
		wc.hCursor = LoadCursor(NULL, IDC_HAND);
		RegisterClassEx(&wc);

        hHiddenWin = CreateWindowEx( 0,
                                     DEF_WINNOTI_CLSNAME,
                                     NULL,
                                     0,
                                     CW_USEDEFAULT,
                                     CW_USEDEFAULT,
                                     0,
                                     0,
                                     HWND_MESSAGE,
                                     NULL,
                                     (struct HINSTANCE__ *)hInstParent,
                                     NULL );
        if ( hHiddenWin != NULL )
        {
            ShowWindow( hHiddenWin, SW_HIDE );

            return true;
        }
    }

    return false;
}

bool WinNotify::destroyHiddenChildWindow()
{
    if ( hHiddenWin != NULL )
    {
        SendMessage( hHiddenWin, WM_CLOSE, 0, 0 );
        DestroyWindow( hHiddenWin );
        hHiddenWin = NULL;

        return true;
    }
    return false;
}

LRESULT CALLBACK hiddenWinHandler(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
    static unsigned params[3] = {0};

    switch(uMsg)
    {
        case DEF_WM_NOTI:
            {
                if ( instNoti != NULL )
                {
                    if ( wParam == DEF_NOTI_UID )
                    {
                        params[0] = wParam;
                        params[1] = LOWORD( lParam );
                        params[2] = HIWORD( lParam );

                        instNoti->MsgEvt( params );

                        return 1;
                    }
                }
            }
            break;

        default:
            return DefWindowProc(hWnd, uMsg, wParam, lParam);
    }

	return 0;
}
