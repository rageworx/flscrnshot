#ifndef __LANGSET_H__
#define __LANGSET_H__

#include <vector>
#include <string>

class LangSet
{
    public:
        typedef enum {
            NONE_ = 0,
            BASE_PATH,
            BASE_PATH_INFO,
            CAPTURE_KEY,
            SELECT_KEY,
            PRECISION_CAPTURE,
            CLIENT_AREA_ONLY,
            PLAY_CAPTURE_SOUND,
            COPY_TO_CLIPBOARD,
            NOTIFIER,
            QUIT_PROGRAM,
            YES,
            NO,
            SELECT_NEW_DIR,
            OPTION,
            CAPTURE_TYPE,
            CAPTYPE_WINDOW_RECT,
            CAPTYPE_WINDOW_ONLY,
            IMAGE_TYPE,
            IMAGE_QUALITY,
            MAX_
        }StringType;
    public:
        LangSet( const wchar_t* bp = NULL, const wchar_t* loc = NULL );
        ~LangSet();

    public:
        bool Load( const wchar_t* loc );
    
    public:
        const char* Get( StringType lst );

    protected:
        bool autofill( const char* scm, const char* str );

    protected:
        wchar_t basepath[512];
        bool    loaded;

    protected:
        std::vector< std::string >  data;
};

#endif /// of __LANGSET_H__
