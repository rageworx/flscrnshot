#ifndef __WINKEYHOOK_H__
#define __WINKEYHOOK_H__

#include <windows.h>

class WinKeyHookEvent
{
    public:
        virtual \
            void OnKeyEvent( unsigned tp, unsigned wp, unsigned lp ) = 0;
};

class WinKeyHook
{
    public:
        WinKeyHook( HINSTANCE hIparent, HWND hWparent, 
                    WinKeyHookEvent* e = NULL );
        ~WinKeyHook();

    public:
        void KEI( unsigned p1, unsigned p2, unsigned p3 );

    public:
        WinKeyHookEvent* event() { return evt; }
        void event( WinKeyHookEvent* e ) { evt = e; }
        HHOOK HookHandle() { return hookHandle; }
        HWND  HookWindow() { return hookedWnd; }

    protected:
        bool createHiddenChildWindow();
        bool destroyHiddenChildWindow();
        bool createHook();
        bool destroyHook();
        void asmspkeys( unsigned state, unsigned key );

    protected:
        WinKeyHookEvent*    evt;
        HHOOK               hookHandle;
        HWND                hookedWnd;
        HINSTANCE           hInstParent;
        HWND                hWndParent;
        HWND                hHiddenWin;
        HANDLE              hMsgLoopThread;
};
#endif /// of __WINKEYHOOK_H__
