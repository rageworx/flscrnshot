#ifndef __CONFIGLOADER_H__
#define __CONFIGLOADER_H__

class ConfigLoader
{
    public:
        ConfigLoader( const char* argv0 = NULL );
        virtual ~ConfigLoader();

    public:
        bool Load();
        bool Reload();
        bool Save();

    // properties ---
    public: 
        void GetWindowPos( int &x, int &y, int &dt );
        void SetWindowPos( int x, int y, int dt );
        int  GetWindowState() { return winstate; }
        void SetWindowState( int v ) { winstate = v; }
        const wchar_t* GetLastPath();
        void SetLastPath( const wchar_t* path );
        int  GetUserMode() { return usermode; }
        void SetUserMode( int um ) { if ( um >= 0 ) usermode = um; }
        unsigned GetCapKey() { return capkey; }
        void SetCapKey( unsigned ck ) { if ( ck >= 0 ) capkey = ck; }
        int  GetOptPrecise() { return opt_precise; }
        void SetOptPrecise( int v ) { opt_precise = v; }
        int  GetOptClientOnly() { return opt_clientonly; }
        void SetOptClientOnly( int v ) { opt_clientonly = v; }
        int  GetOptClipboard() { return opt_clipboard; }
        void SetOptClipboard( int v ) { opt_clipboard = v; }
        int  GetOptPlaySound() { return opt_playsound; }
        void SetOptPlaySound( int v ) { opt_playsound = v; }
        int  GetOptNotify() { return opt_notify; }
        void SetOptNotify( int v ) { opt_notify = v; }
        int  GetOptWriteType() { return opt_writetype; }
        void SetOptWriteType( int v ) { opt_writetype = v; }
        int  GetOptWriteQuality() { return opt_writequality; }
        void SetOptWriteQuality( int v ) { opt_writequality = v; }
        int  GetOptCaptureType() { return opt_captype; }
        void SetOptCaptureType( int v ) { opt_captype = v; }

    protected:
        void clearall();
        void getbasepath();

    protected:
        bool        loaded;
        int         wx;
        int         wy;
        int         dt;
        int         winstate;
        int         usermode;
        unsigned    capkey;
        int         opt_precise;
        int         opt_clientonly;
        int         opt_clipboard;
        int         opt_playsound;
        int         opt_notify;
        int         opt_writetype;
        int         opt_writequality;
        int         opt_captype;
        wchar_t     lastpath[512];

    protected:
        const char* pargv0;
        wchar_t     basepath[512];
};

#endif /// of __CONFIGLOADER_H__
