#include "ltools.h"

#include <cstdio>
#include <cstdlib>
#include <string>

////////////////////////////////////////////////////////////////////////////////

using namespace std;

////////////////////////////////////////////////////////////////////////////////

const wchar_t* extractFileExtension( const wchar_t* file_name )
{
    if ( file_name == NULL )
        return NULL;

    static wstring fnameext;

    fnameext = file_name;

    size_t fname_len = fnameext.size();
    size_t extpos = fnameext.find_last_of( L'.' );

    if ( extpos != wstring::npos )
    {
        fnameext = fnameext.substr( extpos );
    }

    return fnameext.c_str();
}

const wchar_t* extractLastPath( const wchar_t* path )
{
    if ( path == NULL )
        return NULL;

    static wstring retname;
    retname = path;

    const wchar_t dirsep = L'\\';

    size_t fname_len = retname.size();
    size_t extpos = retname.find_last_of( dirsep );

    if ( extpos == fname_len )
    {
        retname = retname.substr( 0, extpos - 1 );
        extpos = retname.find_last_of( dirsep );
    }

    if ( extpos != string::npos )
    {
        retname = retname.substr( extpos + 1 );
    }

    return retname.c_str();
}

bool stringCompare( const wstring &left, const wstring &right )
{
    for( wstring::const_iterator lit = left.begin(), rit = right.begin(); lit != left.end() && rit != right.end(); ++lit, ++rit )
    {
        if( tolower( *lit ) < tolower( *rit ) )
        {
            return true;
        }
        else
        if( tolower( *lit ) > tolower( *rit ) )
        {
            return false;
        }
    }

    if( left.size() < right.size() )
        return true;

    return false;
}

vector<wstring> split(const wstring& s, wchar_t seperator)
{
    vector<wstring> output;
    string::size_type prev_pos = 0;
    string::size_type pos = 0;

    while( ( pos = s.find(seperator, pos)) != wstring::npos )
    {
        wstring substring( s.substr(prev_pos, pos-prev_pos) );

        output.push_back(substring);

        prev_pos = ++pos;
    }

    output.push_back( s.substr( prev_pos, pos-prev_pos ) ); // Last word

    return output;
}

unsigned findstring( std::vector<std::wstring> &src, std::wstring findstr )
{
    for( int cnt=0; cnt<src.size(); cnt++ )
    {
        if ( src[cnt] == findstr )
            return cnt;
    }

    return 0;
}

bool decodeURLpath( std::wstring &path )
{
    if ( path.size() == 0 )
        return false;

    std::wstring tmp = path;
    std::wstring ret;

    // check is this URL type?
    int fpos = tmp.find_first_of( L"file://" );
    if ( fpos != string::npos )
    {
        // Erase it ...
        tmp.erase( fpos, 7 );
    }
    else
    {
        // Just nothing to do.
        return false;
    }

    // Changes %XX codes ...
    for( int cnt=0; cnt<tmp.size(); cnt++ )
    {
        if ( ( tmp[cnt] == L'%' ) && ( ( cnt+2 ) < tmp.size() ) )
        {
            wchar_t nums[] = L"0x  ";
            nums[2] = tmp[cnt+1];
            nums[3] = tmp[cnt+2];
            
            wchar_t newchar = (wchar_t)wcstol(nums, NULL, 16);
            ret += newchar;

            cnt+=2;
        }
        else
        {
            ret += tmp[cnt];
        }
    }

    if ( ret.size() > 0 )
    {
        path = ret;

        return true;
    }

    return false;
}


