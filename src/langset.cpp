#include <unistd.h>
#include <cstdio>
#include <cstdlib>
#include <locale>

#include <tinyxml2.h>
#include "langset.h"
#include "dtools.h"

using namespace std;
using namespace tinyxml2;

//////////////////////////////////////////////////////////////////////////////

const char* defaultSets[]
{
    "None",
    "Base path:",
    "Capture path will combines as {BASEPATH}\\{WINDOWCLASS}[{WINDOWTITLE}]",
    "Capture key:",
    "Select a key",
    "Precision capture",
    "Client area only",
    "Play capture sound",
    "Copy to clipboard",
    "Appears windows notifier when selected option 'Capture only window'",
    "Quit %s?",
    "YES",
    "NO",
    "Select a new directory",
    "Configure options",
    "Capture Type:",
    "Capture all visible window area (with anything visible on window)",
    "Capture only window (only active window surface, without decoration)",
    "Image type:",
    "Image qaulity:"
};

extern const char* _wc2utf8( const wchar_t* src );

//////////////////////////////////////////////////////////////////////////////

bool loadfile( const wchar_t* fn, char* &out, unsigned &outsz )
{
    if ( _waccess( fn, 0 ) == 0 )
    {
        FILE* fp = _wfopen( fn, L"rb" );
        if ( fp != NULL )
        {
            fseek( fp, 0L, SEEK_END );
            unsigned fsz = ftell( fp );
            fseek( fp, 0L, SEEK_SET );

            out = new char[ fsz ];
            if ( out != NULL )
            {
                fread( out, 1, fsz, fp );
                fclose( fp );

                outsz = fsz;

                return true;
            }
        }
    }

    return false;
}

//////////////////////////////////////////////////////////////////////////////

LangSet::LangSet( const wchar_t* bp, const wchar_t* loc )
    : loaded( false )
{
    data.resize( StringType::MAX_ );
    for( unsigned cnt=0; cnt<StringType::MAX_; cnt++ )
    {
        data[cnt] = defaultSets[cnt];
    }

    memset( basepath, 0, 512 * sizeof( wchar_t ) );

    if ( bp != NULL )
    {
        wcsncpy( basepath, bp, 512 );

        unsigned bpsz = wcslen( basepath );
        if ( basepath[bpsz - 1] != L'\\' )
        {
            basepath[bpsz] = L'\\';
        }
    }

    loaded = Load( loc );

    if ( loaded == false )
    {
        // test langs dir ..
        wcsncpy( basepath, L"langs\\", 512 );
        loaded = Load( loc );
    }
}

LangSet::~LangSet()
{
    //Truncate data array.
    if ( data.size() > 0 )
    {
        vector< string >().swap( data );
    }
}

bool LangSet::Load( const wchar_t* loc )
{
    if ( loc == NULL )
        return false;

    char* buff = NULL;
    unsigned buffsz = 0;
    wchar_t strloadfn[512] = {0};
    
    swprintf( strloadfn, L"%S%S.xml", basepath, loc );
 
    if ( loadfile( strloadfn, buff, buffsz ) == false )
    {
        return false;
    }

    XMLDocument* xmldoc = new XMLDocument;
    if ( xmldoc != NULL )
    {
        XMLError retx = xmldoc->Parse( buff, buffsz );
        if ( retx == XML_SUCCESS )
        {
            bool retb = false;

            XMLElement* lroot = xmldoc->FirstChildElement( "language" );
            if ( lroot != NULL )
            {
                XMLElement* lscm = lroot->FirstChildElement( "scheme" );

                while( lscm != NULL )
                {
                    const char* scm = lscm->Attribute( "name" );
                    const char* str = lscm->Attribute( "string" );

                    autofill( scm, str );

                    lscm = lscm->NextSiblingElement( "scheme" );
                }

                retb = true;

                delete[] buff;
            }

            delete xmldoc;

            return retb;
        }

        delete xmldoc;
    }

    delete[] buff;

    return false;
}

const char* LangSet::Get( StringType lst )
{
    int idx = (int)lst;

    if ( idx < StringType::MAX_ )
    {
        return data[idx].c_str();
    }

    return NULL;
}

bool LangSet::autofill( const char* scm, const char* str )
{
    if ( ( scm != NULL ) && ( str != NULL ) )
    {
        StringType st = StringType::NONE_;

        string s_ = scm;

        if ( s_ == "basepath" ) st = StringType::BASE_PATH;
        else
        if ( s_ == "basepathinfo" ) st = StringType::BASE_PATH_INFO;
        else
        if ( s_ == "capturekey" ) st = StringType::CAPTURE_KEY;
        else
        if ( s_ == "selectkey" ) st = StringType::SELECT_KEY;
        else
        if ( s_ == "precisioncapture" ) st = StringType::PRECISION_CAPTURE;
        else
        if ( s_ == "clientareaonly" ) st = StringType::CLIENT_AREA_ONLY;
        else
        if ( s_ == "playcapturesound" ) st = StringType::PLAY_CAPTURE_SOUND;
        else
        if ( s_ == "copytoclipboard" ) st = StringType::COPY_TO_CLIPBOARD;
        else
        if ( s_ == "notifier" ) st = StringType::NOTIFIER;
        else
        if ( s_ == "quitprogram" ) st = StringType::QUIT_PROGRAM;
        else
        if ( s_ == "yes" ) st = StringType::YES;
        else
        if ( s_ == "no" ) st = StringType::NO;
        else
        if ( s_ == "selectnewdirectory" ) st = StringType::SELECT_NEW_DIR;
        else
        if ( s_ == "option" ) st = StringType::OPTION;
        else
        if ( s_ == "captype" ) st = StringType::CAPTURE_TYPE;
        else
        if ( s_ == "captype1" ) st = StringType::CAPTYPE_WINDOW_RECT;
        else
        if ( s_ == "captype2" ) st = StringType::CAPTYPE_WINDOW_ONLY;
        else
        if ( s_ == "imagetype" ) st = StringType::IMAGE_TYPE;
        else
        if ( s_ == "imagequality" ) st = StringType::IMAGE_QUALITY;


        if ( st != StringType::NONE_ )
        {
            data[ (int)st ] = str;

            return true;
        }
    }

    return false;
}

