#include "winkeyhook.h"
#include <cstdio>
#include <cstdlib>
#include <cstring>

//////////////////////////////////////////////////////////////////////////

static WinKeyHook* instanceMe = NULL;

//////////////////////////////////////////////////////////////////////////

LRESULT CALLBACK MsgHookDlgProc(int nCode, WPARAM wParam, LPARAM lParam );

//////////////////////////////////////////////////////////////////////////

WinKeyHook::WinKeyHook( HINSTANCE hIparent, HWND hWparent, WinKeyHookEvent* e )
 : hInstParent( hIparent ),
   hWndParent( hWparent ),
   hookHandle( NULL ),
   hookedWnd( NULL ),
   hMsgLoopThread( INVALID_HANDLE_VALUE ),
   evt( e )
{
    if( createHiddenChildWindow() == true )
    {
        createHook();
    }

    instanceMe = this;
}

WinKeyHook::~WinKeyHook()
{
    destroyHook();
    destroyHiddenChildWindow();

    instanceMe = NULL;
}
        
void WinKeyHook::KEI( unsigned p1, unsigned p2, unsigned p3 )
{
    if ( evt != NULL )
    {
        evt->OnKeyEvent( p1, p2, p3 );
    }
}

bool WinKeyHook::createHiddenChildWindow()
{
    if ( hookedWnd == NULL )
    {
        hookedWnd = hWndParent;
        return true;
    }

    return false;
}

bool WinKeyHook::destroyHiddenChildWindow()
{
    if ( hookedWnd != NULL )
    {
        hookedWnd = NULL;
        return true;
    }
    return false;
}

bool WinKeyHook::createHook()
{
    if ( hookedWnd != NULL )
    {
        hookHandle = SetWindowsHookEx( WH_KEYBOARD_LL,
                                       MsgHookDlgProc,
                                       NULL, 
                                       0 );
        if ( hookHandle != NULL )
            return true;
    }

    return false;
}

bool WinKeyHook::destroyHook()
{
    if ( hookHandle != NULL )
    {
        if ( UnhookWindowsHookEx( hookHandle ) == S_OK )
        {
            hookHandle = NULL;
        }
        
        return true;
    }
    return false;
}

LRESULT CALLBACK MsgHookDlgProc(int nCode, WPARAM wParam, LPARAM lParam )
{
    if ( instanceMe == NULL )
        return 0;

    if ( nCode == 0 )
    {
        if ( instanceMe != NULL )
        {
            switch( wParam )
            {
                case WM_KEYDOWN:
                case WM_KEYUP:
                    // process event.
                    {
                        KBDLLHOOKSTRUCT ks = *((KBDLLHOOKSTRUCT*)lParam);

                        /*
                         *   0x11 22 3333
                         *     -- -- ----
                         *     1  2  3
                         *
                         *     1 : right extended key ?
                         *     2 : scan code ( 0~255 )
                         *     3 : M$ virtual key code
                        */
                        unsigned sk = ks.vkCode;
                        sk |= ( ks.scanCode & 0x000000FF ) << 16;
                        sk |= ( ks.flags & LLKHF_EXTENDED ) << 24;

                        instanceMe->KEI( ks.scanCode,
                                         wParam,
                                         sk );
                    }
                    break;
            }
        }
    }

    return ( CallNextHookEx( instanceMe->HookHandle(), 
                             nCode, wParam, lParam ) );
}
