#include <FL/Fl.H>
#include <FL/Fl_Tooltip.H>
#include <FL/fl_ask.H>
#include "fl_imgtk.h"

#include <string>
#include <locale>

#if ( FL_API_VERSION < 10304 )
    #error "Error, FLTK ABI need 1.3.4 or above"
#endif

#if ( FL_IMGTK_VERSION < 303000 )
    #error "Error, Need FL_IMGTK better build ( 0.3.30.x )"
#endif

#include "winMain.h"

using namespace std;

////////////////////////////////////////////////////////////////////////////////

#define DEF_APP_CLSNAME         "flscrnshot"

////////////////////////////////////////////////////////////////////////////////

HANDLE hMutex = NULL;

const char* convLoc = NULL;
const char* convLng = "Segoe UI";

char* argv_me_path = NULL;
char* argv_me_bin  = NULL;

////////////////////////////////////////////////////////////////////////////////

void procAutoLocale()
{
    LANGID currentUIL = GetSystemDefaultLangID();

    switch( currentUIL & 0xFF )
    {
        case LANG_KOREAN:
            convLoc = "ko_KR.utf8";
            convLng = "Malgun Gothic";
            break;

        case LANG_CZECH:
            convLoc = "cs_CZ.utf8";
            break;

        case LANG_DANISH:
            convLoc = "da_DK.utf8";
            break;

        case LANG_DUTCH:
            convLoc = "nl_NL.utf8";
            break;

        case LANG_ESTONIAN:
            convLoc = "et_EE.utf8";
            break;

        case LANG_FRENCH:
            convLoc = "fr_FR.utf8";
            break;

        case LANG_GERMAN:
            convLoc = "de_DE.utf8";
            break;

        case LANG_GREEK:
            convLoc = "el_GR.utf8";
            break;

        case LANG_JAPANESE:
            convLoc = "ja_JP.utf8";
            convLng = "Meiryo UI";
            break;

        case LANG_CHINESE:
            convLoc = "zh_CN.utf8";
            convLng = "MS YaHei UI";
            break;

        case LANG_CHINESE_TRADITIONAL:
            convLoc = "zh_TW.utf8";
            convLng = "MS JhengHei UI";
            break;

        case LANG_ITALIAN:
            convLoc = "it_IT.utf8";
            break;

        case LANG_RUSSIAN:
            convLoc = "ru_RU.utf8";
            break;

        case LANG_SPANISH:
            convLoc = "es_ES.utf8";
            break;

        case LANG_UZBEK: /// May latin.
            convLoc = "uz_UZ.utf8";
            break;

        default:
            convLoc = "C.utf8";
            break;
    }

	setlocale( LC_ALL, convLoc );
}

void parserArgvZero( const char* argv0 )
{
    string argvextractor = argv0;

#ifdef _WIN32
    char splitter[] = "\\";
#else
    char splitter[] = "/";
#endif

    if ( argvextractor.size() > 0 )
    {
        string::size_type lastSplitPos = argvextractor.rfind( splitter );

        string extracted_path = argvextractor.substr(0, lastSplitPos + 1 );
        string extracted_name = argvextractor.substr( lastSplitPos + 1 );

        argv_me_path = strdup( extracted_path.c_str() );
        argv_me_bin  = strdup( extracted_name.c_str() );
    }
}

void presetFLTKenv()
{
    Fl::set_font( FL_FREE_FONT, convLng );
    Fl_Double_Window::default_xclass( DEF_APP_CLSNAME );

#ifdef __linux__
    fl_message_font_ = FL_HELVETICA;
#else
    fl_message_font_ = FL_FREE_FONT;
#endif // __linux__
    fl_message_size_ = 11;

    Fl_Tooltip::color( 0x15151500 ); // was fl_darker( FL_DARK3 )
    Fl_Tooltip::textcolor( 0xFF663300 );
    Fl_Tooltip::size( 12 );
    Fl_Tooltip::font( fl_message_font_ );
    //Fl_Tooltip::delay( 0.1f );
    Fl_Tooltip::enable( 1 );

    Fl::scheme( "flat" );
}

int main (int argc, char ** argv)
{
    // test Mutex.
    hMutex = CreateMutexA( NULL, FALSE, DEF_APP_CLSNAME );

    if ( GetLastError() == ERROR_ALREADY_EXISTS )
    {
        CloseHandle( hMutex );

        HWND hFound = FindWindowA( DEF_APP_CLSNAME, NULL );

        if ( hFound != NULL )
        {
            DWORD proc_tid = GetWindowThreadProcessId( hFound, NULL );
            DWORD proc_cur = GetCurrentThreadId();
            if ( proc_tid != proc_cur )
            {
                if ( AttachThreadInput( proc_cur, proc_tid, TRUE ) > 0 )
                {                    
                    BringWindowToTop( hFound );
                    AttachThreadInput( proc_cur, proc_tid, FALSE );
                }
            }
        }

        return 0;
    }

    int reti = 0;

    parserArgvZero( argv[0] );
    presetFLTKenv();

    Fl::lock();

    procAutoLocale();

    WMain* pWMain = new WMain( argc, argv, convLoc );

    if ( pWMain != NULL )
    {
        reti = pWMain->Run();
        
        delete pWMain;
    }

    if ( argv_me_path != NULL )
    {
        free( argv_me_path );
    }

    if ( argv_me_bin != NULL )
    {
        free( argv_me_bin );
    }
    
    ReleaseMutex( hMutex );

    return reti;
}
