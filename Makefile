# Makefile for FLSCRNSHOT for MinGW-W64 + M-SYS2
# ----------------------------------------------------------------------
# Written by Raph.K.
#

# Compiler preset.
CC_PREFIX = $(PREFIX)
CC_PATH =

GCC = $(CC_PATH)$(CC_PREFIX)gcc
GPP = $(CC_PATH)$(CC_PREFIX)g++
AR  = ar
WRC = windres
FCG = fltk-config --use-images

# FLTK configs 
FLTKCFG_CXX := $(shell ${FCG} --cxxflags)
FLTKCFG_LFG := $(shell ${FCG} --ldflags)
FLTKCFG_LFG := $(filter-out -lfltk_jpeg, $(FLTKCFG_LFG))

# Base PATH
BASE_PATH = .
FIMG_PATH = ../fl_imgtk/lib
MINI_PATH = ../minIni/dev
SRC_PATH  = $(BASE_PATH)/src
LOCI_PATH = /usr/local/include
LOCL_PATH = /usr/local/lib

# TARGET settings
TARGET_PKG = flscrnshot
TARGET_DIR = ./bin
TARGET_OBJ = ./obj
LIBWIN32S  = -lole32 -luuid -lcomctl32 -lwsock32 -lm -lgdi32 -luser32 -lkernel32 -lShlwapi -lcomdlg32 -lwinmm

# DEFINITIONS
DEFS  = -D_POSIX_THREADS -DSUPPORT_DRAGDROP
DEFS += -DWIN32 -D_WIN32 -DUNICODE -D_UNICODE -DSUPPORT_WCHAR 
DEFS += -D_LARGEFILE_SOURCE -D_LARGEFILE64_SOURCE -D_THREAD_SAFE -D_REENTRANT
DEFS += -DINI_ANSIONLY
DEFS += -DUSE_OMP

# Compiler optiops 
COPTS  = -m64 -mms-bitfields -mwindows 
COPTS += -ffast-math -fexceptions -fopenmp -O3 -s

# CC FLAGS
CFLAGS  = -I$(SRC_PATH)
CFLAGS += -I$(MINI_PATH)
CFLAGS += -I$(FIMG_PATH) -I$(LOCI_PATH) -Ires
CFLAGS += $(FLTKCFG_CXX)
CFLAGS += $(DEFS)
CFLAGS += $(COPTS)

# Windows Resource Flags
WFLGAS  = $(CFLAGS)

# LINK FLAG
LFLAGS  =
LFLAGS += -L$(FIMG_PATH)
LFLAGS += -L$(LOCL_PATH)
LFLAGS += -static
LFLAGS += -lfl_imgtk
LFLAGS += ${FLTKCFG_LFG}
LFLAGS += -lpthread
LFLAGS += $(LIBWIN32S)
LFLAGS += -ltinyxml2
LFLAGS += -lturbojpeg

# minIni Sources & Objects
MININI_SRC = $(MINI_PATH)/minIni.c
MININI_OBJ = $(TARGET_OBJ)/minIni.o

# Sources
SRCS = $(wildcard $(SRC_PATH)/*.cpp)

# Windows resource
WRES = res/resource.rc

# Make object targets from SRCS.
OBJS = $(SRCS:$(SRC_PATH)/%.cpp=$(TARGET_OBJ)/%.o)
WROBJ = $(TARGET_OBJ)/resource.o

.PHONY: prepare clean

all: prepare continue packaging

continue: $(TARGET_DIR)/$(TARGET_PKG)

prepare:
	@mkdir -p $(TARGET_DIR)
	@mkdir -p $(TARGET_OBJ)

clean:
	@echo "Cleaning built targets ..."
	@rm -rf $(TARGET_DIR)/$(TARGET_PKG).*
	@rm -rf $(TARGET_INC)/*.h
	@rm -rf $(TARGET_OBJ)/*.o

packaging:
	@cp -rf LICENSE ${TARGET_DIR}
	@cp -rf readme.md ${TARGET_DIR}

$(MININI_OBJ): $(MININI_SRC)
	@echo "Building $@ ... "
	@$(GCC) $(CFLAGS) -c $< -o $@

$(OBJS): $(TARGET_OBJ)/%.o: $(SRC_PATH)/%.cpp
	@echo "Building $@ ... "
	@$(GPP) $(CFLAGS) -c $< -o $@

$(WROBJ): $(WRES) res/resource.h
	@echo "Building windows resource ..."
	@$(WRC) -i $(WRES) $(WFLAGS) -o $@

$(TARGET_DIR)/$(TARGET_PKG): $(OBJS) $(MININI_OBJ) $(WROBJ)
	@echo "Generating $@ ..."
	@$(GPP) $(TARGET_OBJ)/*.o $(CFLAGS) $(LFLAGS) -o $@
	@echo "done."
