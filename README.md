# FLTK Screenshot for Windows.

## Required to build source.
1. MSYS2 + MinGW-W64 ( included Windows SDK. )
1. Lastest version of fl_imgtk ( https://github.com/rageworx/fl_imgtk/ )
1. Latest version of fltk-1.4.0.9 ( https://github.com/rageworx/fltk-custom )
1. Compuphase minIni ( https://github.com/compuphase/minIni )
1. Leethomanson TinyXML2 ( https://github.com/leethomason/tinyxml2 )
1. libturbojpeg ( https://libjpeg-turbo.org/Main/HomePage )

## Updates in latest version 

* Version 0.1.9.20
    1. Fixed memory leakage that belong to lastest version of FLTK and fl_imgtk.

## Previous updates

* Version 0.1.8.17
    1. Save configuration failure case fixed, on Asian user name of Windows.

* Version 0.1.7.13
    1. Updated FLTK (GUI) engine version to 1.4.0.9, latest changes.
	1. Fixed new cpature directory selection not works correctly.

* Version 0.1.7.12
    1. Updated FLTK (GUI) engine version to 1.4.0.9
	1. Fixed new cpature directory selection not works correctly.

* Version 0.1.7.10
    1. Updated FLTK (GUI) engine version to 1.4.0.7
	1. Bugs fixed bad capture result on Windows High DPI.
	1. Window visible state everytime write to configuration.

* Version 0.1.6.5
    1. Updated FLTK (GUI) engine version to 1.4.0.6
	1. Updated fl_imgtk engine version to 0.3.39.27
	1. Supporting Windows High DPI

* Version 0.1.6.2
    1. Fixed some languages,
	1. Fixed notifying method - some Windows not works by fake security app like Ahnlab, or Nx whatever.


* Version 0.1.6.1
    1. Fixed a bug, capture type 1 ( capture from window only ) ignores options for captutre type 0.
	1. Capture type 1 only captures client area.

* Version 0.1.6.0
    1. Improved options,
	1. JPEG will be supported ( currently not implemented )
	1. Windows notifier enabled with option.
	1. 2 different of capture types
    1. Fixed some bugs, improved some.

* Version 0.1.5.0
    1. Supporting multi languages.
    1. Fixed some bugs, improved some.

* Version 0.1.4.11
    1. Fixed a bug, reload resets all configurations.

* Version 0.1.4.10
    1. Fixed a bug not updates UI when capture key being changed.
	1. Fixed hide, show mechanism.
	1. Compatibility manifest updated.

* Version 0.1.4.9
    1. New window appears when right click to notifier icon.

* Version 0.1.4.7
    1. Notifier icon, hidden function fo displaying notifying message.
	1. Right click will asking quit program on notifier icon.
	1. Reload embodied.

* Version 0.1.4.0
    1. Now plays a shutter sound. ( see external license )
	1. Copying base path to clipboard when it clicked or selected.

* Version 0.1.3.6
    1. Write file path using window title name.

* Version 0.1.3.5
    1. Bugs fixes - 
	    1. abnormal termination
		1. last path not written in config.
	1. Stability improved a little.

* Version 0.1.3.0
    1. Small updates,
	1. Now capture key will shows as human readable.
	1. Show progress while write PNG.

* Version 0.1.2.8
    1. Actual working version for Windows 10.
	1. Tested with all GDI and Direct2D surfaced windows.

* Version 0.1.0.0
    1. First commit.
	
* Version 0.0.0.0
    1. Starts in none.
## Supported OS
1. Windows NT 6.1 or above of 64bit.
1. Some Windows native API will not supported at old OS.

## Supported languages
1. You can make own language by this rule.
```
LANG_KOREAN:"ko_KR.utf8";
LANG_CZECH:"cs_CZ.utf8";
LANG_DANISH:"da_DK.utf8";
LANG_DUTCH:"nl_NL.utf8";
LANG_ESTONIAN:"et_EE.utf8";
LANG_FRENCH:"fr_FR.utf8";
LANG_GERMAN:"de_DE.utf8";
LANG_GREEK:"el_GR.utf8";
LANG_JAPANESE:"ja_JP.utf8";
LANG_CHINESE:"zh_CN.utf8";
LANG_CHINESE_TRADITIONAL: "zh_TW.utf8";
LANG_ITALIAN:"it_IT.utf8";
LANG_RUSSIAN: "ru_RU.utf8";
LANG_SPANISH: "es_ES.utf8";
LANG_UZBEK: "uz_UZ.utf8";
```
1. Korean may `ko_KR.utf8.xml`.
1. German may `de_DE.utf8.xml`.

## Bugs
1. Sometimes may unstable, still in progress to be enhanced.
1. Some OpenGL games not able to capture - key hooking not works.

## External license
1. TinyXML2 by Leethomason at https://github.com/leethomason/tinyxml2
1. Camera shutter sound belongs to http://notification-sounds.com/1729-camera-shutter-sound.html.
1. Converted MP3 to WAV with Audacity, stereo to mono.
