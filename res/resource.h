#ifndef NOCPUID
#include <cpuid.h>
#endif /// of NOCPUID

#define APP_VERSION                 0,1,9,20
#define APP_VERSION_STR             "0.1.9.20"

#define RES_FILEDESC                "FLTK Screenshot for Windows"

#define IDC_ICON_A                  101
#define IDC_ICON_T                  102

